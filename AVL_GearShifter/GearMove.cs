﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;

namespace AVL_GearShifter
{
    public partial class GearMove : DevExpress.XtraEditors.XtraForm
    {
        GearInfo gearInfo;
        public GearMove(GearInfo gearInfo)
        {
            InitializeComponent();
            this.gearInfo = gearInfo;
            var geartypes = gearInfo.GetGearTypes();

            if (geartypes == null)
            {
                return;
            }
            
            gearInfo.Update_Status();

            gridControl1.DataSource = geartypes[gearInfo.gearTypes.CurrentGearTypeNumber].GearPositionList;
        }

        private void GearSettings_SizeChanged(object sender, EventArgs e)
        {
            btnHoming.Width = btnControlOn.Width = btnControlOff.Width = pnTopLeftLeft.Width / 3;
        }

        string prevCurrentGear = string.Empty;
        private void timerUpdate_Tick(object sender, EventArgs e)
        {
            tbAxisXStatus.Text = gearInfo.CurrentGearStatus.AxisXStatusWord.enumStatusWord.ToString();
            tbAxisYStatus.Text = gearInfo.CurrentGearStatus.AxisYStatusWord.enumStatusWord.ToString();
            tbAxisXCurrentPosition.Text = gearInfo.CurrentGearStatus.AxisXActualPosition.ToString();
            tbAxisYCurrentPosition.Text = gearInfo.CurrentGearStatus.AxisYActualPosition.ToString();

            if (prevCurrentGear != gearInfo.StrCurrentGear)
            {
                prevCurrentGear = gearInfo.StrCurrentGear;

                gridControl1.BeginUpdate();
                gridControl1.EndUpdate();
            }
            
        }

        private void btnHoming_Click(object sender, EventArgs e)
        {
            gearInfo.Homing();
        }

        private void btnControlOn_Click(object sender, EventArgs e)
        {
            gearInfo.ControlOn();
        }

        private void btnControlOff_Click(object sender, EventArgs e)
        {
            gearInfo.ControlOff();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (e.Column.FieldName.Equals("StringPrevGear") || e.Column.FieldName.Equals("StringNextGear"))
            {
                var currentGearType = gearInfo.GetGearTypes()[gearInfo.gearTypes.CurrentGearTypeNumber];

                SelectPrevandNextGears form = new SelectPrevandNextGears(currentGearType, gridView1.FocusedRowHandle, e.Column.FieldName);
                form.ShowDialog();
            }
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var currentGearType = gearInfo.GetGearTypes()[gearInfo.gearTypes.CurrentGearTypeNumber];
            List<GearPosition> usageGears = new List<GearPosition>();

            foreach (GearPosition gp in currentGearType.GearPositionList)
            {
                if (gp.Usage)
                {
                    usageGears.Add(gp);
                }
            }

            string tartgetGearName = usageGears[gridView1.FocusedRowHandle].Name;

            gearInfo.MoveGear(tartgetGearName);
        }

        private void AssignCurrentPositionToSelectedGearPosition(int SelectedRowNumber)
        {
            var currentGearType = gearInfo.GetGearTypes()[gearInfo.gearTypes.CurrentGearTypeNumber];
            currentGearType.GearPositionList[gridView1.FocusedRowHandle].ActualPositionX = gearInfo.CurrentGearStatus.AxisXActualPosition;
            currentGearType.GearPositionList[gridView1.FocusedRowHandle].ActualPositionY = gearInfo.CurrentGearStatus.AxisYActualPosition;

            gridControl1.BeginUpdate();
            gridControl1.EndUpdate();
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            var view = sender as GridView;
            string fieldName = view.FocusedColumn.FieldName;
            int rowindex = view.FocusedRowHandle;
            string[] disableEditingColumns = { "Name", "ActualPositionX", "ActualPositionY", "StringPrevGear", "StringNextGear", "CurrentGear" };

            if (disableEditingColumns.Contains(fieldName))
            {
                e.Cancel = true;
            }

            //Todo
            
        }

        private void GearSettings_FormClosed(object sender, FormClosedEventArgs e)
        {

        }


        private void gridView1_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            var row = gridView1.GetDataRow(e.RowHandle);
            var geartypes = gearInfo.GetGearTypes();
            
            if (e.RowHandle >= 0 &&
                e.Column.FieldName == "CurrentGear" &&
                geartypes[gearInfo.gearTypes.CurrentGearTypeNumber].GearPositionList[e.RowHandle].Name == gearInfo.StrCurrentGear)
            {
                e.Appearance.BackColor = Color.Green;
            }
            else
            {
                e.Appearance.BackColor = Color.White;
            }
            
        }

        private void btnErrorAcknowledge_Click(object sender, EventArgs e)
        {
            gearInfo.ErrorAcknowledge();
            gearInfo.EStopOff();
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridView1_CustomRowFilter(object sender, DevExpress.XtraGrid.Views.Base.RowFilterEventArgs e)
        {
            var view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
            bool Usage = (bool)view.GetListSourceRowCellValue(e.ListSourceRow, "Usage");
            if (Usage == true)
                return;

            e.Visible = false;
            e.Handled = true;
        }
    }
}