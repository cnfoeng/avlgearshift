﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using ACS.SPiiPlusNET;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using DevExpress.XtraEditors;
namespace AVL_GearShifter
{
    public enum enumMainState
    {
        NotReadytoSwitchOn = 0,
        SwitchOnDisabled = 1,
        ReadyToSwitchOn = 2,
        SetupError = 3,
        Error = 4,
        HWTests = 5,
        ReadyToOperate = 6,
        Empty = 7,
        OperationEnabled = 8,
        Homing = 9,
        ClearanceCheck = 10,
        GoingtoInitialPosition = 11,
        Aborting = 12,
        Freezing = 13,
        QuickStop = 14,
        GoingtoPosition = 15,
        JoggingPlus = 16,
        JoggingMinus = 17,
        Linearizing = 18,
        PhaseSearch = 19,
        SpecialMode = 20,

    }

    public enum enumStatusWord
    {
        NotConnect = 0,
        NotReady = 1,
        OperationEnable_ControlOn = 2,
        OperationEnable_ControlOff = 3,
        Else = 4,
        Error = 5,
        FatalError = 6
    }

    public class GearCurrentStatus
    {
        int axisXActualPosition;
        int axisYActualPosition;
        public int AxisXActualPosition { get => axisXActualPosition; set => axisXActualPosition = value; }
        public int AxisYActualPosition { get => axisYActualPosition; set => axisYActualPosition = value; }

        public StatusWord AxisXStatusWord = new StatusWord();
        public StatusWord AxisYStatusWord = new StatusWord();

    }

    public class StatusWord
    {
        public int StateVarInt;
        public enumMainState enumMainState;
        public int SubState;

        public enumStatusWord enumStatusWord;
        public int StatusWordInt;
        public int HomingBit;
        public int ErrorBit;
        public int FatalErrorBit;
        public int InTargetPositionBit;
    }

    [Serializable]
    public class GearTypes : List<GearType>
    {
        int currentGearTypeNumber = -1;
        public int CurrentGearTypeNumber { get => currentGearTypeNumber; set => currentGearTypeNumber = value; }
    }

    [Serializable]
    public class GearType
    {
        string name;
        public List<GearPosition> GearPositionList = new List<GearPosition>();

        int homePosX = 0;
        int homePosY = 0;

        public string Name { get => name; set => name = value; }
        public int HomePosX { get => homePosX; set => homePosX = value; }
        public int HomePosY { get => homePosY; set => homePosY = value; }

        public GearType(string gearName)
        {
            this.name = gearName;
        }

        public void ListRenew()
        {
            foreach (GearPosition gp in GearPositionList)
            {
                if (gp.Prev != null)
                {
                    if (gp.Name == gp.Prev.Name)
                    {
                        gp.Prev = gp;
                    }
                }
                if (gp.Next != null)
                {
                    for (int i = 0; i < gp.Next.Count; i++)
                    {
                        if (gp.Name == gp.Next[i].Name)
                        {
                            gp.Next[i] = gp;
                        }
                    }
                }
            }
        }
    }

    [Serializable]
    public class GearPosition
    {
        string name;
        int actualPositionX = -1;
        int actualPositionY = -1;
        bool backtoPrev;
        bool backtoNext;
        bool usage;
        bool mark;
        int conversionValue;
        public string Name { get => name; set => name = value; }
        public int ActualPositionX { get => actualPositionX; set => actualPositionX = value; }
        public int ActualPositionY { get => actualPositionY; set => actualPositionY = value; }
        public bool BacktoPrev { get; set; }
        public bool BacktoNext { get => backtoNext; set => backtoNext = value; }
        public bool Usage { get => usage; set => usage = value; }
        public bool Mark { get => mark; set => mark = value; }
        public int ConversionValue { get => conversionValue; set => conversionValue = value; }

        public GearPosition Prev;
        public List<GearPosition> Next;

        public string StringPrevGear
        {
            get
            {
                if (Prev == null)
                {
                    return null;
                }

                string str = Prev.Name;

                return str;
            }
        }
        public string StringNextGear
        {
            get
            {
                string str = string.Empty;

                if (Next == null)
                {
                    return null;
                }

                foreach (GearPosition gp in Next)
                {
                    if (str == string.Empty)
                    {
                        str = gp.Name;
                    }
                    else
                    {
                        str = str + ", " + gp.Name;
                    }
                }

                return str;
            }
        }


        public GearPosition()
        {
            RemoveMark();
        }

        public void SetGearPosition(string Name, int ActualPositionX, int ActualPositionY, GearPosition Prev, List<GearPosition> Next, bool BacktoPrev, bool BacktoNext, bool Usage)
        {
            this.Name = Name;
            this.ActualPositionX = ActualPositionX;
            this.ActualPositionY = ActualPositionY;
            this.Prev = Prev;
            this.Next = Next;
            this.BacktoPrev = BacktoPrev;
            this.BacktoNext = BacktoNext;
            this.Usage = Usage;
        }//안씀

        public void SetGearPosition(int ActualPositionX, int ActualPositionY)
        {
            this.ActualPositionX = ActualPositionX;
            this.ActualPositionY = ActualPositionY;
        }//안씀

        public void Copy(GearPosition gp)
        {
            this.Name = gp.Name;
            this.ActualPositionX = gp.ActualPositionX;
            this.ActualPositionY = gp.ActualPositionY;
            this.Prev = gp.Prev;
            this.Next = gp.Next;
            this.BacktoPrev = gp.BacktoPrev;
            this.BacktoNext = gp.BacktoNext;
            this.Usage = gp.Usage;
            this.Mark = gp.Mark;
        }

        public void RemoveMark()
        {
            this.Mark = true;
            if (Prev != null)
            {
                while (Prev.Mark == false)
                {
                    Prev.Mark = true;
                    Prev.RemoveMark();
                }
            }

            if (Next != null)
            {
                for (int i = 0; i < Next.Count; i++)
                {
                    while (Next[i].Mark == false)
                    {
                        Next[i].Mark = true;
                        Next[i].RemoveMark();
                    }
                }
            }
        }
    }

    public class GearInfo
    {
        public enum Axis
        {
            X = 0,
            Y = 1
        }
        string strCurrentGear;
        string stringTartgetGear;
        GearType currentGearType;
        string lastCommandGear = "";

        int absCMDCount = 256;
        int homingCMDCount = 145;
        int stopCMDCount = 369;
        int resetCMDCount = 241;
        int demCMDCount = 273;

        public string StrCurrentGear { get => strCurrentGear; set => strCurrentGear = value; }
        public string StringTartgetGear { get => stringTartgetGear; }

        bool bMotorError = false;
        bool bGearRun = false;
        bool bConvert = false;
        bool bEStop = false;

        public GearTypes gearTypes;

        ACSCom acsCom;

        public GearCurrentStatus CurrentGearStatus = new GearCurrentStatus();

        public void EmptyTargetGear()
        {
            StrCurrentGear = null;
        }

        public GearTypes GetGearTypes()
        {
            if (gearTypes != null)
            {
                return gearTypes;
            }
            else
            {
                gearTypes = new GearTypes();
                return gearTypes;
            }
        }

        public void AddGearType(GearType gearType)
        {
            if (gearTypes == null)
            {
                gearTypes = new GearTypes();
            }

            gearTypes.Add(gearType);

        }

        public void SetACSCommunication(ACSCom acsCom)
        {
            this.acsCom = acsCom;
        }

        public void SaveData()
        {
            if (gearTypes == null)
            {
                return;
            }

            FileStream fileStreamObject;
            string FileName = Application.StartupPath + "\\Gear.dat";
            fileStreamObject = new FileStream(FileName, FileMode.Create);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fileStreamObject, gearTypes);
            fileStreamObject.Close();//반응2초 컨트롤 온오프 리모트로. 타겟도달못해도 에러비트.

        }

        public void LoadData()
        {
            try
            {
                FileStream fileStreamObject;
                string FileName = Application.StartupPath + "\\Gear.dat";
                fileStreamObject = new FileStream(FileName, FileMode.Open);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                object obj = binaryFormatter.Deserialize(fileStreamObject);
                fileStreamObject.Close();

                this.gearTypes = (GearTypes)obj;
            }
            catch
            {
                this.gearTypes = new GearTypes();
            }
        }

        public void Update_Status()//빨주 노초 파보 갈검 회흰
        {
            Read_LinmotInput();
            Check_CurrentPosition();
        }

        List<string> CommandList = new List<string>();
        Stack<GearPosition> stack = new Stack<GearPosition>();
        Thread thGear = null;

        List<string> MovingList = new List<string>();
        object MovingLock = new Object();

        public void KillMoveGearThread()
        {

        }
        public void MoveGear(string TargetGearName)
        {
            if (MovingList.Count > 0 && MovingList[MovingList.Count - 1] == TargetGearName)
            {
                //MovingList.RemoveAt(0);
                return;
            }
            else
            {
                lock (MovingLock)
                {
                    MovingList.Add(TargetGearName);
                }

                if (!bGearRun || thGear == null)
                {
                    bGearRun = true;
                    thGear = new Thread(new ThreadStart(MoveGearThread));
                    try
                    {
                        thGear.Start();
                    }
                    catch (Exception)
                    {
                        Debug.Print("ThreadStart 에러");
                    }
                }
            }
        }
        private void MoveGearThread()
        {
            try
            {
                string targetGearName = null;

                while (bGearRun)
                {
                    Application.DoEvents();
                    lock (MovingLock)
                    {
                        try
                        {
                            if (MovingList.Count > 0)
                            {
                                targetGearName = MovingList[0];
                                MovingList.RemoveAt(0);
                            }
                            else
                            {
                                targetGearName = null;
                                bGearRun = false;
                            }
                        }
                        catch
                        {
                            targetGearName = null;
                            bGearRun = false;
                        }
                        //원래 lock } 위치//315추가
                        if (targetGearName != null)
                        {

                            Update_Status();
                            //커런트 M 인데 타겟 M-로 어긋난 상태. gearRun = true에서 false로 안됨. 새로 밸류들어와도 타겟기어 변하지 않는 상태.//315추가
                            if (StrCurrentGear == targetGearName)
                            {
                                lastCommandGear = targetGearName;
                            }
                            else if (stack.Count != 0 || lastCommandGear == targetGearName)
                            {
                                bGearRun = false;//315추가
                                return;
                            }
                            else
                            {
                                lastCommandGear = targetGearName;
                                stringTartgetGear = targetGearName;
                                stack = new Stack<GearPosition>();
                                GearPosition CurrentGearPosition = FindGearObjectFromGearName(strCurrentGear);
                                CurrentGearPosition.RemoveMark();
                                stack.Push(CurrentGearPosition);//시작지점 넣고 시작
                                if (bEStop)
                                {
                                    FindPath(CurrentGearPosition, "N");
                                }
                                else
                                {
                                    FindPath(CurrentGearPosition, targetGearName);
                                }
                                //이 후에 경로가 stack에 역순으로 들어가 있음.
                                stack = ReverseStack(stack);

                                while (stack.Count != 0)
                                {
                                    CurrentGearPosition = absMove_XY(CurrentGearPosition, stack);
                                    System.Threading.Thread.Sleep(100);

                                    Read_LinmotInput();
                                    System.Threading.Thread.Sleep(10);

                                    DateTime StartTime = DateTime.Now;
                                    Boolean expired = false;

                                    do
                                    {
                                        Update_Status();

                                        Application.DoEvents();
                                        if (DateTime.Now.Subtract(StartTime).TotalMilliseconds >= 500)
                                            expired = true;
                                    } while ((CurrentGearStatus.AxisXStatusWord.InTargetPositionBit == 0 || CurrentGearStatus.AxisYStatusWord.InTargetPositionBit == 0) && !expired && !Check_MotorError());

                                    if (expired)
                                    {
                                        //stop();

                                        //XtraMessageBox.Show("<size=14>시간 초과.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }

                                }

                                Update_Status();

                                //ControlOff();

                                foreach (GearPosition gp in currentGearType.GearPositionList)
                                {
                                    gp.RemoveMark();
                                }


                            }
                        }
                    }
                }
                bGearRun = false;
                thGear.Abort();
                thGear.Join();
                thGear = null;
            }
            catch (Exception ex)
            {
                bGearRun = false;
                thGear.Abort();
                thGear.Join();
                thGear = null;
            }
        }

        public bool Check_MotorError()
        {
            if (CurrentGearStatus.AxisXStatusWord.ErrorBit == 1 || CurrentGearStatus.AxisYStatusWord.ErrorBit == 1)
            {
                Debug.Print("Gear 서보드라이버 에러 비트 ON");
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CheckInTargetPosition()
        {
            Stopwatch sw = new Stopwatch();

            sw.Start();

            while (CurrentGearStatus.AxisXStatusWord.InTargetPositionBit == 0 || CurrentGearStatus.AxisYStatusWord.InTargetPositionBit == 0)
            {
                if (sw.ElapsedMilliseconds > 1000)
                {
                    Stop();
                    break;


                }
            }
        }

        private GearPosition absMove_XY(GearPosition CurrentGearPosition, Stack<GearPosition> stack)
        {

            GearPosition NextGear;
            NextGear = stack.Pop();
            if (NextGear.Next == null || NextGear.Next.Count == 0)
            {
                if (NextGear.BacktoPrev == true)
                {
                    stack.Push(NextGear.Prev);
                }
            }
            if (NextGear.Prev == null)
            {
                if (NextGear.BacktoNext == true)
                {
                    stack.Push(NextGear.Next[0]);
                }
            }

            int DifferenceX = Math.Abs(CurrentGearPosition.ActualPositionX - NextGear.ActualPositionX);
            int DifferenceY = Math.Abs(CurrentGearPosition.ActualPositionY - NextGear.ActualPositionY);
            int NextPosition;
            Axis SelectXY;
            if (DifferenceX > DifferenceY) //X축 이동
            {
                NextPosition = NextGear.ActualPositionX;
                SelectXY = Axis.Y;
            }
            else if (DifferenceX < DifferenceY)
            {
                NextPosition = NextGear.ActualPositionY;
                SelectXY = Axis.X;
            }
            else//스택에 들어있는 게 같은 포지션인 경우 행동X
            {
                return NextGear;
            }

            absCMDCount++;
            if (absCMDCount == 271)
            {
                absCMDCount = 257;
            }


            int[] Value = new int[6] { 0, absCMDCount, NextPosition, 1000000, 300000, 300000 };

            int i;
            i = 2;
            acsCom.WriteLinmotOutput(Value[i], SelectXY, i);
            i = 5;
            acsCom.WriteLinmotOutput(Value[i], SelectXY, i);
            i = 3;
            acsCom.WriteLinmotOutput(Value[i], SelectXY, i);
            i = 4;
            acsCom.WriteLinmotOutput(Value[i], SelectXY, i);
            i = 1;
            acsCom.WriteLinmotOutput(Value[i], SelectXY, i);

            System.Threading.Thread.Sleep(50);


            return NextGear;

        }

        private GearPosition FindPath(GearPosition CurrentGearPosition, string TargetName)
        {
            bool FlagContain = true;

            if (CurrentGearPosition.Name == TargetName)
            {
                return null;//찾음
            }

            if (CurrentGearPosition.Prev != null)//1
            {
                foreach (GearPosition gp in stack)
                {
                    FlagContain = true;
                    if (gp.Name == CurrentGearPosition.Prev.Name)
                    {
                        FlagContain = false;
                        break;
                    }
                }

                if (FlagContain && CurrentGearPosition.Prev.Mark == true) //2
                {
                    stack.Push(CurrentGearPosition.Prev);
                    Debug.Print("Push" + CurrentGearPosition.Prev.Name);
                    return FindPath(CurrentGearPosition.Prev, TargetName);
                }
                else//3
                {


                    if (CurrentGearPosition.Next == null || CurrentGearPosition.Next.Count == 0)//4
                    {
                        CurrentGearPosition.Mark = false;
                        stack.Pop();
                        Debug.Print("Pop" + CurrentGearPosition.Name);

                        foreach (GearPosition gp in CurrentGearPosition.Prev.Next)
                        {
                            if (gp.Name == CurrentGearPosition.Name)
                            {
                                gp.Copy(CurrentGearPosition);
                                break;
                            }
                        }
                        return FindPath(CurrentGearPosition.Prev, TargetName);
                    }
                    else
                    {//5


                        foreach (GearPosition Next in CurrentGearPosition.Next)//Next갈길 있는경우 return으로 빠져나감. 갈길 없는경우 계속 진행돼서 다음 코드 실행.
                        {
                            foreach (GearPosition gp in stack)
                            {
                                FlagContain = true;

                                if (gp.Name == Next.Name)
                                {
                                    FlagContain = false;
                                    break;
                                }
                            }

                            if (FlagContain && Next.Mark == true)
                            {
                                stack.Push(Next);
                                Debug.Print("Push" + Next.Name);
                                return FindPath(Next, TargetName);
                            }
                        }

                        //6
                        CurrentGearPosition.Mark = false;
                        stack.Pop();
                        Debug.Print("Pop" + CurrentGearPosition.Name);
                        if (CurrentGearPosition.Next != null)
                        {
                            for (int i = 0; i < CurrentGearPosition.Next.Count; i++)
                            {
                                if (CurrentGearPosition.Next[i].Mark == true)
                                {
                                    FlagContain = false;
                                    CurrentGearPosition.Next[i].Prev = CurrentGearPosition;
                                    return FindPath(CurrentGearPosition.Next[i], TargetName);
                                }


                            }

                            if (FlagContain)
                            {
                                foreach (GearPosition gp in CurrentGearPosition.Prev.Next)
                                {
                                    if (gp.Name == CurrentGearPosition.Name)
                                    {
                                        gp.Copy(CurrentGearPosition);
                                        break;
                                    }
                                }
                                return FindPath(CurrentGearPosition.Prev, TargetName);
                            }



                        }
                        else//끝자리인 경우 Prev로 돌아간다.
                        {
                            foreach (GearPosition gp in CurrentGearPosition.Prev.Next)
                            {
                                if (gp.Name == CurrentGearPosition.Name)
                                {
                                    gp.Copy(CurrentGearPosition);
                                    break;
                                }
                            }
                            return FindPath(CurrentGearPosition.Prev, TargetName);
                        }
                        return null;

                    }
                }

            }
            else//7
            {
                if (CurrentGearPosition.Next == null)//8
                {
                    return null;//시작점 1개만 있는 경우
                }
                else
                {//9

                    foreach (GearPosition Next in CurrentGearPosition.Next)//Next갈길 있는경우 return으로 빠져나감. 갈길 없는경우 계속 진행돼서 다음 코드 실행.
                    {
                        foreach (GearPosition gp in stack)
                        {
                            if (gp.Name == Next.Name)
                            {
                                FlagContain = false;
                                break;
                            }

                        }


                        if (FlagContain && Next.Mark == true)
                        {
                            stack.Push(Next);
                            return FindPath(Next, TargetName);
                        }
                    }

                    //10
                    CurrentGearPosition.Mark = false;
                    stack.Pop();
                    Debug.Print("Pop" + CurrentGearPosition.Name);
                    for (int i = 0; i < CurrentGearPosition.Next.Count; i++)
                    {
                        CurrentGearPosition.Next[i].Prev = CurrentGearPosition;
                        return FindPath(CurrentGearPosition.Next[i], TargetName);
                    }


                    return null;

                }
            }
        }

        public void Read_LinmotInput()
        {
            try
            {
                int[] Value = acsCom.ReadLinmotInput();

                CurrentGearStatus.AxisXActualPosition = Value[10];
                CurrentGearStatus.AxisYActualPosition = Value[4];
                CurrentGearStatus.AxisXStatusWord.StateVarInt = Value[6];
                CurrentGearStatus.AxisYStatusWord.StateVarInt = Value[0];
                CurrentGearStatus.AxisXStatusWord.enumMainState = CheckMainState(Value[6]);
                CurrentGearStatus.AxisYStatusWord.enumMainState = CheckMainState(Value[0]);
                CurrentGearStatus.AxisXStatusWord.SubState = Value[6] & 0xFF;
                CurrentGearStatus.AxisYStatusWord.SubState = Value[0] & 0xFF;
                CurrentGearStatus.AxisXStatusWord.StatusWordInt = Value[7];
                CurrentGearStatus.AxisYStatusWord.StatusWordInt = Value[1];
                ChangeStatusWordStringtoBit(CurrentGearStatus.AxisXStatusWord);
                ChangeStatusWordStringtoBit(CurrentGearStatus.AxisYStatusWord);
            }
            catch { }
        }
        private void ChangeStatusWordStringtoBit(StatusWord StatusWord)
        {
            string tempByte;

            tempByte = Convert.ToString(StatusWord.StatusWordInt, 2).PadLeft(16, '0');
            StatusWord.ErrorBit = int.Parse(tempByte[12].ToString());
            StatusWord.InTargetPositionBit = int.Parse(tempByte[5].ToString());
            StatusWord.HomingBit = int.Parse(tempByte[4].ToString());
            StatusWord.FatalErrorBit = int.Parse(tempByte[3].ToString());

            if (StatusWord.StatusWordInt == 0)
            {
                StatusWord.enumStatusWord = enumStatusWord.NotConnect;
            }
            else if (StatusWord.StatusWordInt == 0x1111)
            {
                StatusWord.enumStatusWord = enumStatusWord.NotReady;
            }
            else if (StatusWord.StatusWordInt == 0x4830 || StatusWord.StatusWordInt == 0x0830)
            {
                StatusWord.enumStatusWord = enumStatusWord.OperationEnable_ControlOff;
            }
            else if (StatusWord.StatusWordInt == 0x4C37 || StatusWord.StatusWordInt == 0x0C37)
            {
                StatusWord.enumStatusWord = enumStatusWord.OperationEnable_ControlOn;
            }
            else
            {
                if (StatusWord.ErrorBit == 1)
                {
                    StatusWord.enumStatusWord = enumStatusWord.Error;
                }
                else if (StatusWord.FatalErrorBit == 1)
                {
                    StatusWord.enumStatusWord = enumStatusWord.FatalError;
                }
                else
                {
                    StatusWord.enumStatusWord = enumStatusWord.Else;
                }
            }
        }
        private enumMainState CheckMainState(int Value)
        {
            int tempMainstate = Value >> 8;
            if (tempMainstate == 0)
            {
                return enumMainState.NotReadytoSwitchOn;
            }
            else if (tempMainstate == 1)
            {
                return enumMainState.SwitchOnDisabled;
            }
            else if (tempMainstate == 2)
            {
                return enumMainState.ReadyToSwitchOn;
            }
            else if (tempMainstate == 3)
            {
                return enumMainState.SetupError;
            }
            else if (tempMainstate == 4)
            {
                return enumMainState.Error;
            }
            else if (tempMainstate == 5)
            {
                return enumMainState.HWTests;
            }
            else if (tempMainstate == 6)
            {
                return enumMainState.ReadyToOperate;
            }
            else if (tempMainstate == 7)
            {
                return enumMainState.Empty;
            }
            else if (tempMainstate == 8)
            {
                return enumMainState.OperationEnabled;
            }
            else if (tempMainstate == 9)
            {
                return enumMainState.Homing;
            }
            else if (tempMainstate == 10)
            {
                return enumMainState.ClearanceCheck;
            }
            else if (tempMainstate == 11)
            {
                return enumMainState.GoingtoInitialPosition;
            }
            else if (tempMainstate == 12)
            {
                return enumMainState.Aborting;
            }
            else if (tempMainstate == 13)
            {
                return enumMainState.Freezing;
            }
            else if (tempMainstate == 14)
            {
                return enumMainState.QuickStop;
            }
            else if (tempMainstate == 15)
            {
                return enumMainState.GoingtoPosition;
            }
            else if (tempMainstate == 16)
            {
                return enumMainState.JoggingPlus;
            }
            else if (tempMainstate == 17)
            {
                return enumMainState.JoggingMinus;
            }
            else if (tempMainstate == 18)
            {
                return enumMainState.Linearizing;
            }
            else if (tempMainstate == 19)
            {
                return enumMainState.PhaseSearch;
            }
            else if (tempMainstate == 20)
            {
                return enumMainState.SpecialMode;
            }
            else
            {
                return enumMainState.Empty;
            }



        }
        private GearPosition FindGearObjectFromGearName(string TargetGearName)
        {
            foreach (GearPosition gp in currentGearType.GearPositionList)
            {
                if (TargetGearName == gp.Name)
                {
                    return gp;
                }
            }
            return null;

        }

        private Stack<GearPosition> ReverseStack(Stack<GearPosition> input)
        {
            Stack<GearPosition> Temp = new Stack<GearPosition>();

            while (input.Count != 0)
            {
                Temp.Push(input.Pop());
            }

            return Temp;
        }

        public void Homing()
        {

            int LinmotSlaveNum = acsCom.ReadLinmotSlaveNum();

            Read_LinmotInput();

            if (LinmotSlaveNum == 0)
            {
                // Not Ready
                if (CurrentGearStatus.AxisYStatusWord.enumStatusWord == enumStatusWord.NotReady)
                {
                    // Motor Power On - Control On Axis X. 축 1개인 경우. X축 Y축 구분이 사실 무의미
                    acsCom.WriteLinmotOutput(63, Axis.X, 0);
                    System.Threading.Thread.Sleep(50);
                    acsCom.WriteLinmotOutput(62, Axis.X, 0);
                    System.Threading.Thread.Sleep(50);
                    acsCom.WriteLinmotOutput(63, Axis.X, 0);
                }

                // 105 ~ 159 명령. 
                homingCMDCount++;
                if (homingCMDCount == 159)
                {
                    homingCMDCount = 145;
                }

                int[] Value = new int[3] { 0, homingCMDCount, currentGearType.HomePosY };

                acsCom.WriteLinmotOutput(Value[2], Axis.X, 2);
                acsCom.WriteLinmotOutput(Value[1], Axis.X, 1);
            }
            else if (LinmotSlaveNum == 1) // 축 2개
            {
                if (CurrentGearStatus.AxisYStatusWord.enumStatusWord == enumStatusWord.NotReady)
                {
                    // Motor Power On - Control On Axis Y
                    acsCom.WriteLinmotOutput(63, Axis.X, 0);
                    System.Threading.Thread.Sleep(50);
                    acsCom.WriteLinmotOutput(62, Axis.X, 0);
                    System.Threading.Thread.Sleep(50);
                    acsCom.WriteLinmotOutput(63, Axis.X, 0);
                }

                homingCMDCount++;

                if (homingCMDCount == 159)
                {
                    homingCMDCount = 145;
                }

                int[] Value = new int[3] { 0, homingCMDCount, currentGearType.HomePosX };

                acsCom.WriteLinmotOutput(Value[2], Axis.X, 2);
                acsCom.WriteLinmotOutput(Value[1], Axis.X, 1);

                if (CurrentGearStatus.AxisXStatusWord.enumStatusWord == enumStatusWord.NotReady)
                {
                    // Motor Power On - Control On Axis X
                    acsCom.WriteLinmotOutput(63, Axis.Y, 0);
                    System.Threading.Thread.Sleep(50);
                    acsCom.WriteLinmotOutput(62, Axis.Y, 0);
                    System.Threading.Thread.Sleep(50);
                    acsCom.WriteLinmotOutput(63, Axis.Y, 0);
                }

                homingCMDCount++;
                if (homingCMDCount == 159)
                {
                    homingCMDCount = 145;
                }
                Value = new int[3] { 0, homingCMDCount, currentGearType.HomePosX };


                acsCom.WriteLinmotOutput(Value[2], Axis.Y, 2);
                acsCom.WriteLinmotOutput(Value[1], Axis.Y, 1);

            }
        }
        public void Reset()
        {
            homingCMDCount++;
            if (homingCMDCount == 159)
            {
                homingCMDCount = 145;
            }

            int[] Value = new int[2] { 0, resetCMDCount };

            acsCom.WriteLinmotOutput(Value[1], Axis.X, 1);

            resetCMDCount++;
            if (resetCMDCount == 159)
            {
                resetCMDCount = 145;
            }

            acsCom.WriteLinmotOutput(Value[1], Axis.Y, 1);
        }

        private void absMove_X(int X)
        {
            absCMDCount++;
            if (absCMDCount == 271)
            {
                absCMDCount = 257;
            }

            int[] Value = new int[6] { 0, absCMDCount, X, 500000, 100000, 100000 };

            int i;
            i = 2;
            acsCom.WriteLinmotOutput(Value[i], Axis.Y, i);
            i = 5;
            acsCom.WriteLinmotOutput(Value[i], Axis.Y, i);
            i = 3;
            acsCom.WriteLinmotOutput(Value[i], Axis.Y, i);
            i = 4;
            acsCom.WriteLinmotOutput(Value[i], Axis.Y, i);
            i = 1;
            acsCom.WriteLinmotOutput(Value[i], Axis.Y, i);
            System.Threading.Thread.Sleep(50);
        }
        private void demMove_X(int X)
        {
            demCMDCount++;
            if (demCMDCount == 287)
            {
                demCMDCount = 273;
            }

            int[] Value = new int[6] { 0, demCMDCount, X, 500000, 100000, 100000 };

            int i;
            i = 2;
            acsCom.WriteLinmotOutput(Value[i], Axis.Y, i);
            i = 5;
            acsCom.WriteLinmotOutput(Value[i], Axis.Y, i);
            i = 3;
            acsCom.WriteLinmotOutput(Value[i], Axis.Y, i);
            i = 4;
            acsCom.WriteLinmotOutput(Value[i], Axis.Y, i);
            i = 1;
            acsCom.WriteLinmotOutput(Value[i], Axis.Y, i);
            System.Threading.Thread.Sleep(50);

        }
        private void absMove_Y(int Y)
        {
            absCMDCount++;
            if (absCMDCount == 271)
            {
                absCMDCount = 257;
            }

            int[] Value = new int[6] { 0, absCMDCount, Y, 500000, 100000, 100000 };


            int i;
            i = 2;
            acsCom.WriteLinmotOutput(Value[i], Axis.X, i);
            i = 5;
            acsCom.WriteLinmotOutput(Value[i], Axis.X, i);
            i = 3;
            acsCom.WriteLinmotOutput(Value[i], Axis.X, i);
            i = 4;
            acsCom.WriteLinmotOutput(Value[i], Axis.X, i);
            i = 1;
            acsCom.WriteLinmotOutput(Value[i], Axis.X, i);
            System.Threading.Thread.Sleep(50);

        }
        private void demMove_Y(int Y)
        {
            demCMDCount++;
            if (demCMDCount == 287)
            {
                demCMDCount = 273;
            }

            int[] Value = new int[6] { 0, demCMDCount, Y, 500000, 100000, 100000 };


            int i;
            i = 2;
            acsCom.WriteLinmotOutput(Value[i], Axis.X, i);
            i = 5;
            acsCom.WriteLinmotOutput(Value[i], Axis.X, i);
            i = 3;
            acsCom.WriteLinmotOutput(Value[i], Axis.X, i);
            i = 4;
            acsCom.WriteLinmotOutput(Value[i], Axis.X, i);
            i = 1;
            acsCom.WriteLinmotOutput(Value[i], Axis.X, i);
            System.Threading.Thread.Sleep(50);

        }


        public void ControlOn()
        {
            ControlOnLogic();
            Thread.Sleep(100);
            ControlOffLogic();
            Thread.Sleep(100);
            ControlOnLogic();
        }

        private void ControlOnLogic()
        {
            int LinmotSlaveNum = acsCom.ReadLinmotSlaveNum();

            Read_LinmotInput();

            if (LinmotSlaveNum == 0)
            {
                acsCom.WriteLinmotOutput(63, Axis.X, 0);
            }
            else if (LinmotSlaveNum == 1)
            {

                acsCom.WriteLinmotOutput(63, Axis.Y, 0);

                acsCom.WriteLinmotOutput(63, Axis.X, 0);

            }
        }

        public void ControlOff()
        {
            ControlOffLogic();
        }

        public void ControlOffLogic()
        {
            int LinmotSlaveNum = acsCom.ReadLinmotSlaveNum();

            Read_LinmotInput();

            if (LinmotSlaveNum == 0)
            {
                acsCom.WriteLinmotOutput(62, Axis.X, 0);
            }
            else if (LinmotSlaveNum == 1)
            {
                acsCom.WriteLinmotOutput(62, Axis.Y, 0);

                acsCom.WriteLinmotOutput(62, Axis.X, 0);
            }
        }
        public void Stop()
        {
            int[] Value;
            int delay = 100;
            Read_LinmotInput();
            int beforeActX = CurrentGearStatus.AxisXActualPosition;
            int beforeActY = CurrentGearStatus.AxisYActualPosition;
            Thread.Sleep(delay);
            acsCom.WriteLinmotOutput(2111, Axis.X, 0);
            Thread.Sleep(delay);
            acsCom.WriteLinmotOutput(63, Axis.X, 0);
            Thread.Sleep(delay);
            Read_LinmotInput();
            acsCom.WriteLinmotOutput(62, Axis.X, 0);
            Thread.Sleep(delay);
            acsCom.WriteLinmotOutput(63, Axis.X, 0);


            acsCom.WriteLinmotOutput(2111, Axis.Y, 0);
            Thread.Sleep(delay);
            acsCom.WriteLinmotOutput(63, Axis.Y, 0);
            Thread.Sleep(delay);
            Read_LinmotInput();
            acsCom.WriteLinmotOutput(62, Axis.Y, 0);
            Thread.Sleep(delay);
            acsCom.WriteLinmotOutput(63, Axis.Y, 0);
            Thread.Sleep(delay);

            Thread.Sleep(500);

            Homing();

            System.Threading.Thread.Sleep(500);

            GearPosition gearPosition = CorrectGearPosition(beforeActX, CurrentGearStatus.AxisYActualPosition);
            if (gearPosition != null)
            {
                int XPosition = gearPosition.ActualPositionX;
                int YPosition = gearPosition.ActualPositionY;

                homingCMDCount++;
                if (homingCMDCount == 159)
                {
                    homingCMDCount = 145;
                }

                Value = new int[3] { 0, homingCMDCount, YPosition };

                acsCom.WriteLinmotOutput(Value[2], Axis.X, 2);
                acsCom.WriteLinmotOutput(Value[1], Axis.X, 1);

                System.Threading.Thread.Sleep(delay);

                homingCMDCount++;

                if (homingCMDCount == 159)
                {
                    homingCMDCount = 145;
                }

                Value = new int[3] { 0, homingCMDCount, XPosition };


                acsCom.WriteLinmotOutput(Value[2], Axis.Y, 2);
                acsCom.WriteLinmotOutput(Value[1], Axis.Y, 1);

                Thread.Sleep(delay);
                Thread.Sleep(delay);

                //CorrectGearPosition();
            }

        }
        private GearPosition CorrectGearPosition(int actX, int actY)
        {
            double min = 0;
            int index = 0;
            for (int i = 0; i < currentGearType.GearPositionList.Count; i++)
            {
                if (i == 0)
                {
                    min = Math.Sqrt(Math.Pow(currentGearType.GearPositionList[i].ActualPositionX - actX, 2) + Math.Pow(currentGearType.GearPositionList[i].ActualPositionY - actY, 2));
                }
                else if (min > Math.Sqrt(Math.Pow(currentGearType.GearPositionList[i].ActualPositionX - actX, 2) + Math.Pow(currentGearType.GearPositionList[i].ActualPositionY - actY, 2))) //10000 = 1cm
                {
                    index = i;
                }
                return currentGearType.GearPositionList[index];
            }

            return null;// Error.
        }
        public void ErrorAcknowledge()
        {
            int LinmotSlaveNum = acsCom.ReadLinmotSlaveNum();

            if (LinmotSlaveNum == 0)
            {
                if (CurrentGearStatus.AxisYStatusWord.enumStatusWord == enumStatusWord.Error)
                {
                    acsCom.WriteLinmotOutput(128, Axis.X, 0);
                    acsCom.WriteLinmotOutput(62, Axis.X, 0);
                }
                else if (CurrentGearStatus.AxisYStatusWord.enumStatusWord == enumStatusWord.FatalError)
                {
                    MessageBox.Show("Fatal Error must restart your instrument.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (LinmotSlaveNum == 1)
            {
                if (CurrentGearStatus.AxisXStatusWord.enumStatusWord == enumStatusWord.Error)
                {
                    acsCom.WriteLinmotOutput(128, Axis.Y, 0);
                    acsCom.WriteLinmotOutput(62, Axis.Y, 0);
                }
                if (CurrentGearStatus.AxisYStatusWord.enumStatusWord == enumStatusWord.Error)
                {
                    acsCom.WriteLinmotOutput(128, Axis.X, 0);
                    acsCom.WriteLinmotOutput(62, Axis.X, 0);
                }

                if (CurrentGearStatus.AxisXStatusWord.enumStatusWord == enumStatusWord.FatalError || CurrentGearStatus.AxisYStatusWord.enumStatusWord == enumStatusWord.FatalError)
                {
                    MessageBox.Show("Fatal Error must restart your instrument.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void Check_CurrentPosition()
        {
            int[] arrDistance = new int[currentGearType.GearPositionList.Count];
            int arrCount = 0;
            int CheckMin = -1;
            int CheckMinRow = 0;

            foreach (GearPosition gp in currentGearType.GearPositionList)
            {
                arrDistance[arrCount] = CheckDistance(gp.ActualPositionX, gp.ActualPositionY, CurrentGearStatus.AxisXActualPosition, CurrentGearStatus.AxisYActualPosition);
                if (CheckMin == -1)
                {
                    CheckMin = arrDistance[arrCount];
                    CheckMinRow = arrCount;
                    StrCurrentGear = gp.Name;
                }
                else
                {
                    if (CheckMin > arrDistance[arrCount])
                    {
                        CheckMin = arrDistance[arrCount];
                        CheckMinRow = arrCount;

                        StrCurrentGear = gp.Name;
                    }
                }
                arrCount++;
            }
        }
        public void SetCurrentGearType()
        {
            if (gearTypes == null || gearTypes.Count == 0)
            {
                return;
            }

            if (gearTypes.CurrentGearTypeNumber == -1 && gearTypes.Count > 1)
            {
                gearTypes.CurrentGearTypeNumber = 0;
            }
            currentGearType = gearTypes[gearTypes.CurrentGearTypeNumber];
        }
        private int CheckDistance(int actX, int actY, int CurrentX, int CurrentY)
        {
            return (int)Math.Sqrt(Math.Pow(actX - CurrentX, 2) + Math.Pow(actY - CurrentY, 2));
        }
        Thread thConvert;
        public void ConvertOn()
        {
            bConvert = true;

            thConvert = new Thread(new ThreadStart(ConvertDItoGearMovement));
            thConvert.Start();
        }

        private void ConvertDItoGearMovement()
        {
            while (bConvert && !bEStop)
            {
                int[] DIArr = acsCom.GetDIDataArray();

                int DIValue = DIArr[0];

                string binaryDI = Convert.ToString(DIValue, 2).PadLeft(5, '0');

                //Conversion Value
                int firstValueBit = (int)Char.GetNumericValue(binaryDI.ElementAt(4));
                int secondValueBit = (int)Char.GetNumericValue(binaryDI.ElementAt(3));
                int thirdValueBit = (int)Char.GetNumericValue(binaryDI.ElementAt(2));

                bool bitSimul = false;

                if (bitSimul)
                {
                    firstValueBit = 1;
                    secondValueBit = 1;
                    thirdValueBit = 1;
                }
                int conversionValue = firstValueBit + (2 * secondValueBit) + (4 * thirdValueBit);

                foreach (GearPosition gp in currentGearType.GearPositionList)
                {
                    if (gp.ConversionValue == conversionValue)
                    {
                        MoveGear(gp.Name);
                        Debug.Print(gp.Name);
                        break;
                    }
                }
                
                Thread.Sleep(500);

                //bool bitSimul2 = false;

                //if (bitSimul2)
                //{
                //    firstValueBit = 1;
                //    secondValueBit = 0;
                //    thirdValueBit = 1;

                //    conversionValue = firstValueBit + (2 * secondValueBit) + (4 * thirdValueBit);

                //    foreach (GearPosition gp in currentGearType.GearPositionList)
                //    {
                //        if (gp.ConversionValue == conversionValue)
                //        {
                //            MoveGear(gp.Name);
                //            Debug.Print(gp.Name);
                //            break;
                //        }
                //    }
                //}
            }
        }

        public void ConvertOff()
        {
            bConvert = false;
            thConvert?.Abort();
            thConvert?.Join();
        }

        public void EStopOn()
        {
            bEStop = true;
        }
        public void EStopOff()
        {
            bEStop = false;
        }
    }
}
