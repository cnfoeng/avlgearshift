﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using DevExpress.UserSkins;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
namespace AVL_GearShifter
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            XtraMessageBox.AllowHtmlText = true;

            bool bnew;
            Mutex mutex = new Mutex(true, "MutexName", out bnew);
            if (bnew)
            {

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                BonusSkins.Register();
                Application.Run(new Main());
                mutex.ReleaseMutex();
            }
            else
            {
                XtraMessageBox.Show("<size=14>프로그램이 이미 실행 중입니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();

            }
        }
    }
}
