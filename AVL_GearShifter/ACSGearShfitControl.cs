﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACS.SPiiPlusNET;

namespace AVL_GearShifter
{
    public partial class ACSCom
    {
        /// <summary>
        ///  
        /// </summary>
        /// <param name="value"></param>
        /// <param name="selectXY">Axis X = 0, Axis Y = 1</param>
        /// <param name="option"></param>
        public void WriteLinmotOutput(int value,GearInfo.Axis axis, int option)
        {
            Ch.WriteVariable(value, "g_eCAT_Linmot_Output", ProgramBuffer.ACSC_NONE, (int)axis, (int)axis, option, option);
        }

        public int[] ReadLinmotInput()
        {
            int[,] Dim2Value = null;
            Dim2Value = (int[,])Ch.ReadVariable("g_eCAT_Linmot_Input", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);
            int[] Value = Dim2Value.Cast<int>().ToArray();

            return Value;
        }

        public int ReadLinmotSlaveNum()
        {
            object obj = Ch.ReadVariable("eCatNetworkLinmotSlaveNum", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);
            int LinmotSlaveNum = (int)obj;

            return LinmotSlaveNum;
        }
        public int ReadErrorBit()
        {
            object obj = Ch.ReadVariable("g_eCAT_DO_Data", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);
            int DOValue = (int)obj;

            return DOValue;
        }
        public void WriteErrorBitToAVL(double value)
        {
            double[] SendData = new double[1] { value };
            Ch.WriteVariable(SendData, "g_eCAT_DO_Data", ProgramBuffer.ACSC_NONE, 0, 0, 0, 0);
        }
    }
}
