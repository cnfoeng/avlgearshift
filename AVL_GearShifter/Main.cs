﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using DevExpress.XtraEditors;

namespace AVL_GearShifter
{
    public partial class Main : DevExpress.XtraEditors.XtraForm
    {
        ACSCom acsCom = new ACSCom();
        GearInfo gearInfo;

        int RemoteBit = 0;

        public Main()
        {
            InitializeComponent();
            StartACSCommunication();
        }
        private void StartACSCommunication()
        {
            if (gearInfo == null)
            {
                gearInfo = new GearInfo();
            }

            gearInfo.LoadData();

            if (acsCom.Start_ACSCommunication("10.0.0.100"))
            {
                acsCom.StartReadDI();//Thread-While

                gearInfo.SetACSCommunication(acsCom);

                timerUpdate.Enabled = true;
            }

            gearInfo.SetCurrentGearType();
        }
        private void Main_SizeChanged(object sender, EventArgs e)
        {
            pnLeft.Width = pnBack.Width / 2;
            pnRight.Width = pnBack.Width / 2;
            pnTopRight.Width = pnBack.Width / 2;
        }

        private void btnManualControl_Click(object sender, EventArgs e)
        {
            //Todo acsCom null이면 접근못하게. 메시지 띄우기

            GearMove form = new GearMove(gearInfo);
            form.ShowDialog();
        }

        private void btnGearSettings_Click(object sender, EventArgs e)
        {
            //Todo acsCom null이면 접근못하게. 메시지 띄우기

            GearSettings form = new GearSettings(gearInfo);
            form.ShowDialog();
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            acsCom.StopReadDIDO();
            gearInfo.SaveData();
        }
        int ControlBit = 0;
        int errorBit = 1;
        int prevOutputValue = -1;
        private void timerUpdate_Tick(object sender, EventArgs e)
        {
            gearInfo.Update_Status();


            tbAxisXStatus.Text = gearInfo.CurrentGearStatus.AxisXStatusWord.enumStatusWord.ToString();
            tbAxisYStatus.Text = gearInfo.CurrentGearStatus.AxisYStatusWord.enumStatusWord.ToString();
            tbCurrentGear.Text = gearInfo.StrCurrentGear;
            tbTargetGear.Text = gearInfo.StringTartgetGear;



            if (acsCom.IsConnected())
            {
                siACSConnection.StateIndex = 3;

                tsConvertOnOff.Enabled = true;

                int[] DIArr = acsCom.GetDIDataArray();

                int DIValue = DIArr[0];

                string binaryDI = Convert.ToString(DIValue, 2).PadLeft(5, '0'); 

                
                //Conversion Value
                int firstValueBit = (int)Char.GetNumericValue(binaryDI.ElementAt(4));
                int secondValueBit = (int)Char.GetNumericValue(binaryDI.ElementAt(3));
                int thirdValueBit = (int)Char.GetNumericValue(binaryDI.ElementAt(2));

                //AVL Value
                //int ControlBit = RemoteBit;
                ControlBit = (int)Char.GetNumericValue(binaryDI.ElementAt(1));//Control On/Off
                int EStopBit = (int)Char.GetNumericValue(binaryDI.ElementAt(0));//E-Stop

                bool bitSimul = false;
                if (bitSimul)
                {
                    firstValueBit = 0;
                    secondValueBit = 0;
                    thirdValueBit = 1;

                    ControlBit = 1;
                   EStopBit = 1;
                }

                int DOValue = acsCom.GetDODataArray()[0];

                string binaryDO = Convert.ToString(DOValue, 2).PadLeft(4, '0');
                

               

                if (ControlBit == 1)
                {
                    tsConvertOnOff.IsOn = true;
                    siAVLControl.StateIndex = 3;
                }
                else
                {
                    tsConvertOnOff.IsOn = false;
                    gearInfo.EStopOff();
                    siAVLControl.StateIndex = 0;
                }

                if (EStopBit == 1)
                {
                    siEStopBit.StateIndex = 3;
                }
                else
                {
                    siEStopBit.StateIndex = 0;
                }

                errorBit = gearInfo.Check_MotorError() ? 0 : 1;


                if (errorBit == 0)
                {
                    siError.StateIndex = 1;
                }
                else
                {
                    siError.StateIndex = 3;
                }

                if (RemoteBit == 1)
                {
                    if (ControlBit == 1)
                    {
                        if (gearInfo.CurrentGearStatus.AxisXStatusWord.enumStatusWord != enumStatusWord.OperationEnable_ControlOn)
                        {
                            gearInfo.ControlOn();
                        }
                    }
                    else
                    {
                        if (gearInfo.CurrentGearStatus.AxisXStatusWord.enumStatusWord != enumStatusWord.OperationEnable_ControlOff)
                        {
                            gearInfo.ControlOff();
                        }
                    }

                    if (EStopBit == 0)
                    {
                        gearInfo.EStopOn();

                        if (gearInfo.StrCurrentGear != "N")
                        {
                            gearInfo.MoveGear("N");
                        }
                    }

                    


                    int conversionValue = firstValueBit + (2 * secondValueBit) + (4 * thirdValueBit);

                    lbValue.Text = $"Digital Input Value : " + conversionValue.ToString();
                }
            }
            else
            {

                siACSConnection.StateIndex = 0;

                tsConvertOnOff.Enabled = false;

                lbValue.Text = $"Digital Input Value : -";
            }

            var currentGearType = gearInfo.GetGearTypes()[gearInfo.gearTypes.CurrentGearTypeNumber];
            int CurrentGearValue = 0;

            foreach (GearPosition gp in currentGearType.GearPositionList)
            {
                if (gp.Name == gearInfo.StrCurrentGear)
                {
                    CurrentGearValue = gp.ConversionValue;
                    break;
                }
            }

            string binary = Convert.ToString(CurrentGearValue, 2).PadLeft(3, '0');

            if (binary.Length != 3)
            {
                return;
            }

            int firstOutputBit = (int)Char.GetNumericValue(binary.ElementAt(2));
            int secondOutputBit = (int)Char.GetNumericValue(binary.ElementAt(1));
            int thirdOutputBit = (int)Char.GetNumericValue(binary.ElementAt(0));
            
            int OutputValue = 8 * errorBit + 4 * thirdOutputBit + 2 * secondOutputBit + 1 * firstOutputBit;

            if (prevOutputValue != OutputValue)
            {
                prevOutputValue = OutputValue;
                acsCom.WriteErrorBitToAVL(OutputValue);
            }
        }

        private void tsConvertOnOff_Toggled(object sender, EventArgs e)
        {
            //if(acsCom)
            
            if (tsConvertOnOff.IsOn)
            {
                if(ControlBit == 0)
                {
                    tsConvertOnOff.IsOn = false;
                    return;
                }
                gearInfo.ConvertOn();
                RemoteBit = 1;
            }
            else
            {
                gearInfo.ConvertOff();
                RemoteBit = 0;
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (XtraMessageBox.Show("<size=14>프로그램을 종료하시겠습니까?</size>", "<size=14>알림</size>", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
    }
}
