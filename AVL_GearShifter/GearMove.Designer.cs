﻿namespace AVL_GearShifter
{
    partial class GearMove
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GearMove));
            this.pnTop = new DevExpress.XtraEditors.PanelControl();
            this.pnTopRight = new DevExpress.XtraEditors.PanelControl();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.tbAxisXStatus = new DevExpress.XtraEditors.TextEdit();
            this.tbAxisYStatus = new DevExpress.XtraEditors.TextEdit();
            this.tbAxisXCurrentPosition = new DevExpress.XtraEditors.TextEdit();
            this.tbAxisYCurrentPosition = new DevExpress.XtraEditors.TextEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.pnTopLeft = new DevExpress.XtraEditors.PanelControl();
            this.pnTopLeftLeft = new DevExpress.XtraEditors.PanelControl();
            this.btnControlOn = new DevExpress.XtraEditors.SimpleButton();
            this.btnControlOff = new DevExpress.XtraEditors.SimpleButton();
            this.btnHoming = new DevExpress.XtraEditors.SimpleButton();
            this.pnFill = new DevExpress.XtraEditors.PanelControl();
            this.panelFillFill = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGearName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXPos = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYPos = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColMove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colCurrentGear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnFillBottom = new DevExpress.XtraEditors.PanelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnErrorAcknowledge = new DevExpress.XtraEditors.SimpleButton();
            this.timerUpdate = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pnTop)).BeginInit();
            this.pnTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnTopRight)).BeginInit();
            this.pnTopRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbAxisXStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAxisYStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAxisXCurrentPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAxisYCurrentPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnTopLeft)).BeginInit();
            this.pnTopLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnTopLeftLeft)).BeginInit();
            this.pnTopLeftLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnFill)).BeginInit();
            this.pnFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelFillFill)).BeginInit();
            this.panelFillFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnFillBottom)).BeginInit();
            this.pnFillBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnTop
            // 
            this.pnTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnTop.Controls.Add(this.pnTopRight);
            this.pnTop.Controls.Add(this.pnTopLeft);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(800, 188);
            this.pnTop.TabIndex = 0;
            // 
            // pnTopRight
            // 
            this.pnTopRight.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnTopRight.Controls.Add(this.dataLayoutControl1);
            this.pnTopRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnTopRight.Location = new System.Drawing.Point(357, 0);
            this.pnTopRight.Name = "pnTopRight";
            this.pnTopRight.Size = new System.Drawing.Size(443, 188);
            this.pnTopRight.TabIndex = 1;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.tbAxisXStatus);
            this.dataLayoutControl1.Controls.Add(this.tbAxisYStatus);
            this.dataLayoutControl1.Controls.Add(this.tbAxisXCurrentPosition);
            this.dataLayoutControl1.Controls.Add(this.tbAxisYCurrentPosition);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.Root;
            this.dataLayoutControl1.Size = new System.Drawing.Size(443, 188);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // tbAxisXStatus
            // 
            this.tbAxisXStatus.Location = new System.Drawing.Point(133, 12);
            this.tbAxisXStatus.Name = "tbAxisXStatus";
            this.tbAxisXStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAxisXStatus.Properties.Appearance.Options.UseFont = true;
            this.tbAxisXStatus.Properties.ReadOnly = true;
            this.tbAxisXStatus.Size = new System.Drawing.Size(298, 30);
            this.tbAxisXStatus.StyleController = this.dataLayoutControl1;
            this.tbAxisXStatus.TabIndex = 4;
            // 
            // tbAxisYStatus
            // 
            this.tbAxisYStatus.Location = new System.Drawing.Point(133, 46);
            this.tbAxisYStatus.Name = "tbAxisYStatus";
            this.tbAxisYStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAxisYStatus.Properties.Appearance.Options.UseFont = true;
            this.tbAxisYStatus.Properties.ReadOnly = true;
            this.tbAxisYStatus.Size = new System.Drawing.Size(298, 30);
            this.tbAxisYStatus.StyleController = this.dataLayoutControl1;
            this.tbAxisYStatus.TabIndex = 5;
            // 
            // tbAxisXCurrentPosition
            // 
            this.tbAxisXCurrentPosition.Location = new System.Drawing.Point(217, 80);
            this.tbAxisXCurrentPosition.Name = "tbAxisXCurrentPosition";
            this.tbAxisXCurrentPosition.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAxisXCurrentPosition.Properties.Appearance.Options.UseFont = true;
            this.tbAxisXCurrentPosition.Properties.ReadOnly = true;
            this.tbAxisXCurrentPosition.Size = new System.Drawing.Size(214, 30);
            this.tbAxisXCurrentPosition.StyleController = this.dataLayoutControl1;
            this.tbAxisXCurrentPosition.TabIndex = 6;
            // 
            // tbAxisYCurrentPosition
            // 
            this.tbAxisYCurrentPosition.Location = new System.Drawing.Point(217, 114);
            this.tbAxisYCurrentPosition.Name = "tbAxisYCurrentPosition";
            this.tbAxisYCurrentPosition.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAxisYCurrentPosition.Properties.Appearance.Options.UseFont = true;
            this.tbAxisYCurrentPosition.Properties.ReadOnly = true;
            this.tbAxisYCurrentPosition.Size = new System.Drawing.Size(214, 30);
            this.tbAxisYCurrentPosition.StyleController = this.dataLayoutControl1;
            this.tbAxisYCurrentPosition.TabIndex = 7;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(443, 188);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.tbAxisXStatus;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(423, 34);
            this.layoutControlItem1.Text = "Axis X Status";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(116, 24);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.tbAxisYStatus;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(423, 34);
            this.layoutControlItem2.Text = "Axis Y Status";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(116, 24);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.tbAxisXCurrentPosition;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 68);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(423, 34);
            this.layoutControlItem3.Text = "Axis X Current Position";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(202, 24);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.Control = this.tbAxisYCurrentPosition;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 102);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(423, 66);
            this.layoutControlItem4.Text = "Axis Y Current Position";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(202, 24);
            // 
            // pnTopLeft
            // 
            this.pnTopLeft.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnTopLeft.Controls.Add(this.pnTopLeftLeft);
            this.pnTopLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnTopLeft.Location = new System.Drawing.Point(0, 0);
            this.pnTopLeft.Name = "pnTopLeft";
            this.pnTopLeft.Size = new System.Drawing.Size(357, 188);
            this.pnTopLeft.TabIndex = 0;
            // 
            // pnTopLeftLeft
            // 
            this.pnTopLeftLeft.Controls.Add(this.btnControlOn);
            this.pnTopLeftLeft.Controls.Add(this.btnControlOff);
            this.pnTopLeftLeft.Controls.Add(this.btnHoming);
            this.pnTopLeftLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnTopLeftLeft.Location = new System.Drawing.Point(0, 0);
            this.pnTopLeftLeft.Name = "pnTopLeftLeft";
            this.pnTopLeftLeft.Size = new System.Drawing.Size(357, 188);
            this.pnTopLeftLeft.TabIndex = 0;
            // 
            // btnControlOn
            // 
            this.btnControlOn.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnControlOn.Appearance.Options.UseFont = true;
            this.btnControlOn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnControlOn.Location = new System.Drawing.Point(118, 2);
            this.btnControlOn.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnControlOn.Name = "btnControlOn";
            this.btnControlOn.Size = new System.Drawing.Size(121, 184);
            this.btnControlOn.TabIndex = 1;
            this.btnControlOn.Text = "Control\r\nOn";
            this.btnControlOn.Click += new System.EventHandler(this.btnControlOn_Click);
            // 
            // btnControlOff
            // 
            this.btnControlOff.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnControlOff.Appearance.Options.UseFont = true;
            this.btnControlOff.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnControlOff.Location = new System.Drawing.Point(239, 2);
            this.btnControlOff.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnControlOff.Name = "btnControlOff";
            this.btnControlOff.Size = new System.Drawing.Size(116, 184);
            this.btnControlOff.TabIndex = 2;
            this.btnControlOff.Text = "Control\r\nOff";
            this.btnControlOff.Click += new System.EventHandler(this.btnControlOff_Click);
            // 
            // btnHoming
            // 
            this.btnHoming.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHoming.Appearance.Options.UseFont = true;
            this.btnHoming.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnHoming.Location = new System.Drawing.Point(2, 2);
            this.btnHoming.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnHoming.Name = "btnHoming";
            this.btnHoming.Size = new System.Drawing.Size(116, 184);
            this.btnHoming.TabIndex = 0;
            this.btnHoming.Text = "Homing";
            this.btnHoming.Click += new System.EventHandler(this.btnHoming_Click);
            // 
            // pnFill
            // 
            this.pnFill.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnFill.Controls.Add(this.panelFillFill);
            this.pnFill.Controls.Add(this.pnFillBottom);
            this.pnFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnFill.Location = new System.Drawing.Point(0, 188);
            this.pnFill.Name = "pnFill";
            this.pnFill.Size = new System.Drawing.Size(800, 382);
            this.pnFill.TabIndex = 1;
            // 
            // panelFillFill
            // 
            this.panelFillFill.Controls.Add(this.gridControl1);
            this.panelFillFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFillFill.Location = new System.Drawing.Point(0, 0);
            this.panelFillFill.Name = "panelFillFill";
            this.panelFillFill.Size = new System.Drawing.Size(800, 316);
            this.panelFillFill.TabIndex = 3;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.LookAndFeel.SkinName = "Office 2013 Light Gray";
            this.gridControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.gridControl1.Size = new System.Drawing.Size(796, 312);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Appearance.Row.Options.UseTextOptions = true;
            this.gridView1.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGearName,
            this.colXPos,
            this.colYPos,
            this.ColMove,
            this.colCurrentGear});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowColumnMoving = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView1.OptionsFilter.AllowFilterEditor = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomRowFilter += new DevExpress.XtraGrid.Views.Base.RowFilterEventHandler(this.gridView1_CustomRowFilter);
            // 
            // colGearName
            // 
            this.colGearName.AppearanceHeader.Options.UseTextOptions = true;
            this.colGearName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGearName.Caption = "Gear Name";
            this.colGearName.FieldName = "Name";
            this.colGearName.Name = "colGearName";
            this.colGearName.OptionsFilter.AllowAutoFilter = false;
            this.colGearName.OptionsFilter.AllowFilter = false;
            this.colGearName.Visible = true;
            this.colGearName.VisibleIndex = 0;
            this.colGearName.Width = 128;
            // 
            // colXPos
            // 
            this.colXPos.AppearanceHeader.Options.UseTextOptions = true;
            this.colXPos.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colXPos.Caption = "Axis X Position";
            this.colXPos.FieldName = "ActualPositionX";
            this.colXPos.Name = "colXPos";
            this.colXPos.OptionsFilter.AllowAutoFilter = false;
            this.colXPos.OptionsFilter.AllowFilter = false;
            this.colXPos.Visible = true;
            this.colXPos.VisibleIndex = 1;
            this.colXPos.Width = 162;
            // 
            // colYPos
            // 
            this.colYPos.AppearanceHeader.Options.UseTextOptions = true;
            this.colYPos.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colYPos.Caption = "Axis Y Position";
            this.colYPos.FieldName = "ActualPositionY";
            this.colYPos.Name = "colYPos";
            this.colYPos.OptionsFilter.AllowAutoFilter = false;
            this.colYPos.OptionsFilter.AllowFilter = false;
            this.colYPos.Visible = true;
            this.colYPos.VisibleIndex = 2;
            this.colYPos.Width = 159;
            // 
            // ColMove
            // 
            this.ColMove.AppearanceHeader.Options.UseTextOptions = true;
            this.ColMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColMove.Caption = "Move";
            this.ColMove.ColumnEdit = this.repositoryItemButtonEdit1;
            this.ColMove.Name = "ColMove";
            this.ColMove.OptionsFilter.AllowAutoFilter = false;
            this.ColMove.OptionsFilter.AllowFilter = false;
            this.ColMove.Visible = true;
            this.ColMove.VisibleIndex = 3;
            this.ColMove.Width = 165;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            editorButtonImageOptions1.SvgImageSize = new System.Drawing.Size(80, 40);
            serializableAppearanceObject1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            serializableAppearanceObject1.Options.UseFont = true;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Move", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // colCurrentGear
            // 
            this.colCurrentGear.AppearanceHeader.Options.UseTextOptions = true;
            this.colCurrentGear.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCurrentGear.Caption = "Current Gear";
            this.colCurrentGear.FieldName = "CurrentGear";
            this.colCurrentGear.Name = "colCurrentGear";
            this.colCurrentGear.OptionsFilter.AllowAutoFilter = false;
            this.colCurrentGear.OptionsFilter.AllowFilter = false;
            this.colCurrentGear.Visible = true;
            this.colCurrentGear.VisibleIndex = 4;
            this.colCurrentGear.Width = 180;
            // 
            // pnFillBottom
            // 
            this.pnFillBottom.Controls.Add(this.btnClose);
            this.pnFillBottom.Controls.Add(this.btnErrorAcknowledge);
            this.pnFillBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnFillBottom.Location = new System.Drawing.Point(0, 316);
            this.pnFillBottom.Name = "pnFillBottom";
            this.pnFillBottom.Size = new System.Drawing.Size(800, 66);
            this.pnFillBottom.TabIndex = 2;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Appearance.Options.UseFont = true;
            this.btnClose.Location = new System.Drawing.Point(667, 10);
            this.btnClose.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.btnClose.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(121, 44);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnErrorAcknowledge
            // 
            this.btnErrorAcknowledge.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnErrorAcknowledge.Appearance.Options.UseFont = true;
            this.btnErrorAcknowledge.Location = new System.Drawing.Point(12, 10);
            this.btnErrorAcknowledge.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.btnErrorAcknowledge.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnErrorAcknowledge.Name = "btnErrorAcknowledge";
            this.btnErrorAcknowledge.Size = new System.Drawing.Size(211, 44);
            this.btnErrorAcknowledge.TabIndex = 2;
            this.btnErrorAcknowledge.Text = "Error Acknowledge";
            this.btnErrorAcknowledge.Click += new System.EventHandler(this.btnErrorAcknowledge_Click);
            // 
            // timerUpdate
            // 
            this.timerUpdate.Enabled = true;
            this.timerUpdate.Interval = 500;
            this.timerUpdate.Tick += new System.EventHandler(this.timerUpdate_Tick);
            // 
            // GearMove
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 570);
            this.Controls.Add(this.pnFill);
            this.Controls.Add(this.pnTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GearMove";
            this.Text = "Gear Move";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.GearSettings_FormClosed);
            this.SizeChanged += new System.EventHandler(this.GearSettings_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pnTop)).EndInit();
            this.pnTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnTopRight)).EndInit();
            this.pnTopRight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbAxisXStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAxisYStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAxisXCurrentPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAxisYCurrentPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnTopLeft)).EndInit();
            this.pnTopLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnTopLeftLeft)).EndInit();
            this.pnTopLeftLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnFill)).EndInit();
            this.pnFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelFillFill)).EndInit();
            this.panelFillFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnFillBottom)).EndInit();
            this.pnFillBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnTop;
        private DevExpress.XtraEditors.PanelControl pnTopLeft;
        private DevExpress.XtraEditors.PanelControl pnTopLeftLeft;
        private DevExpress.XtraEditors.PanelControl pnTopRight;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit tbAxisXStatus;
        private DevExpress.XtraEditors.TextEdit tbAxisYStatus;
        private DevExpress.XtraEditors.TextEdit tbAxisXCurrentPosition;
        private DevExpress.XtraEditors.TextEdit tbAxisYCurrentPosition;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.PanelControl pnFill;
        private DevExpress.XtraEditors.SimpleButton btnControlOn;
        private DevExpress.XtraEditors.SimpleButton btnHoming;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colGearName;
        private DevExpress.XtraGrid.Columns.GridColumn colXPos;
        private DevExpress.XtraGrid.Columns.GridColumn colYPos;
        private DevExpress.XtraGrid.Columns.GridColumn ColMove;
        private System.Windows.Forms.Timer timerUpdate;
        private DevExpress.XtraEditors.PanelControl panelFillFill;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.PanelControl pnFillBottom;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnErrorAcknowledge;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentGear;
        private DevExpress.XtraEditors.SimpleButton btnControlOff;
    }
}