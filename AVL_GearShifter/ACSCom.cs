﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACS.SPiiPlusNET;
using System.Diagnostics;
using System.Collections;
using System.Threading;

namespace AVL_GearShifter
{
    public partial class ACSCom
    {
        Api Ch;

        int analogInputArraySize = 10;
        int digitalInputArraySize = 10;

        int[] analogInputArray;
        int[] digitalInputArray;
        int[] digitalOutputArray;

        bool bReadAI = false;
        bool bReadDIDO = false;

        Thread thReadAI;
        Thread thReadDIDO;
        
        public ACSCom()
        {

        }

        public bool Start_ACSCommunication(string ipAddress)
        {
            try
            {
                // Create new object of class Channel
                // Type Channel is defined in SPiiPlusCOM650 Type Library
                Ch = new Api();
                object pWait = 0;
                string cIP = ipAddress;
                int ComType = 0;

                int Protocol = (int)EthernetCommOption.ACSC_SOCKET_STREAM_PORT;
                // Open ethernet communuication.
                // RemoteAddress.Text defines the controller's TCP/IP address.
                // Protocol is TCP/IP in case of network connection, and UDP in case of point-to-point connection.
                // string a = Ethercat.sysInfo.HardWareInfo[0].

                if (ComType == 3)
                {
                    Ch.OpenCommSimulator();
                }
                else
                {
                    Ch.OpenCommEthernet(cIP, Protocol);
                }
                
                if (Ch.IsConnected)
                {
                    Debug.Print("ACS 연결 성공");
                    return true;

                }
                else
                {
                    Debug.Print("ACS 연결 실패");
                    return false;
                }

            }
            catch (ACS.SPiiPlusNET.ACSException e)
            {
                Debug.Print("ACS 초기화 실패. 장비 연결 확인 필요");
                Debug.Print(e.ToString());
                return false;
            }

        }
       
        public bool IsConnected()
        {
            return Ch.IsConnected;
        }
        public void StartReadAI()
        {
            bReadAI = true;
            thReadAI = new Thread(new ThreadStart(Read_AI));
            thReadAI.Start();
        }
        public void StopReadAI()
        {
            bReadAI = false;
            thReadAI?.Abort();
            thReadAI?.Join();
        }
        public void StartReadDI()
        {
            bReadDIDO = true;
            thReadDIDO = new Thread(new ThreadStart(Read_DIDO));
            thReadDIDO.Start();
        }
        public void StopReadDIDO()
        {
            bReadDIDO = false;
            Thread.Sleep(100);
            thReadDIDO?.Abort();
            thReadDIDO?.Join();
        }
        public int[] GetAIDataArray()
        {
            return analogInputArray;
        }
        public int[] GetDIDataArray()
        {
            return digitalInputArray;
        }
        public int[] GetDODataArray()
        {
            return digitalOutputArray;
        }
        private void Read_AI()
        {
            while (bReadAI)
            {
                try
                {
                    object Result;
                    Result = Ch.ReadVariable("g_eCAT_AI_Data", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);

                    var array = (Result as IEnumerable).Cast<object>().Select(x => (int)x).ToArray();
                    analogInputArray = array;
                }
                catch (ACSException e)
                {
                    Debug.Print("Read AI 에러");
                    Debug.Print(e.ToString());
                }

                Thread.Sleep(200);
            }
        }

        private void Read_DIDO()
        {
            while (bReadDIDO)
            {
                try
                {
                    object Result;
                    Result = Ch.ReadVariable("g_eCAT_DI_Data", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);

                    var array = (Result as IEnumerable).Cast<object>().Select(x => (int)x).ToArray();
                    digitalInputArray = array;

                    Result = Ch.ReadVariable("g_eCAT_DO_Data", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);

                    array = (Result as IEnumerable).Cast<object>().Select(x => (int)x).ToArray();
                    digitalOutputArray = array;
                }
                catch (ACSException e)
                {
                    Debug.Print("Read DIDO 에러");
                    Debug.Print(e.ToString());
                }

                Thread.Sleep(50);
            }
        }

        
    }
}
