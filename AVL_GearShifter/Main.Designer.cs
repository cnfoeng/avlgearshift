﻿namespace AVL_GearShifter
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState17 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState18 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState19 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState20 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState21 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState22 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState23 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState24 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState25 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState26 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState27 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState28 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState29 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState30 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState31 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState32 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            this.gaugeControl1 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge1 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.siACSConnection = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.pnLeft = new DevExpress.XtraEditors.PanelControl();
            this.btnManualControl = new DevExpress.XtraEditors.SimpleButton();
            this.pnRight = new DevExpress.XtraEditors.PanelControl();
            this.btnGearSettings = new DevExpress.XtraEditors.SimpleButton();
            this.pnBack = new DevExpress.XtraEditors.PanelControl();
            this.pnTop = new DevExpress.XtraEditors.PanelControl();
            this.pnTopRight = new DevExpress.XtraEditors.PanelControl();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.tbAxisYStatus = new DevExpress.XtraEditors.TextEdit();
            this.tbAxisXStatus = new DevExpress.XtraEditors.TextEdit();
            this.tbTargetGear = new DevExpress.XtraEditors.TextEdit();
            this.tbCurrentGear = new DevExpress.XtraEditors.TextEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.pnTopLeft = new DevExpress.XtraEditors.PanelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.gaugeControl5 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge5 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.siAVLControl = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.gaugeControl4 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge4 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.siError = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.gaugeControl3 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge3 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.siEStopBit = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.lbValue = new DevExpress.XtraEditors.LabelControl();
            this.tsConvertOnOff = new DevExpress.XtraEditors.ToggleSwitch();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.timerUpdate = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.siACSConnection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnLeft)).BeginInit();
            this.pnLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnRight)).BeginInit();
            this.pnRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnBack)).BeginInit();
            this.pnBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnTop)).BeginInit();
            this.pnTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnTopRight)).BeginInit();
            this.pnTopRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbAxisYStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAxisXStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTargetGear.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCurrentGear.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnTopLeft)).BeginInit();
            this.pnTopLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.siAVLControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.siError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.siEStopBit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tsConvertOnOff.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gaugeControl1
            // 
            this.gaugeControl1.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gaugeControl1.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge1});
            this.gaugeControl1.Location = new System.Drawing.Point(139, 69);
            this.gaugeControl1.Name = "gaugeControl1";
            this.gaugeControl1.Size = new System.Drawing.Size(36, 42);
            this.gaugeControl1.TabIndex = 4;
            // 
            // stateIndicatorGauge1
            // 
            this.stateIndicatorGauge1.Bounds = new System.Drawing.Rectangle(6, 6, 24, 30);
            this.stateIndicatorGauge1.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.siACSConnection});
            this.stateIndicatorGauge1.Name = "stateIndicatorGauge1";
            // 
            // siACSConnection
            // 
            this.siACSConnection.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(124F, 124F);
            this.siACSConnection.Name = "stateIndicatorComponent1";
            this.siACSConnection.Size = new System.Drawing.SizeF(200F, 200F);
            this.siACSConnection.StateIndex = 0;
            indicatorState17.Name = "State1";
            indicatorState17.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState18.Name = "State2";
            indicatorState18.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState19.Name = "State3";
            indicatorState19.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState20.Name = "State4";
            indicatorState20.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.siACSConnection.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState17,
            indicatorState18,
            indicatorState19,
            indicatorState20});
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(10, 78);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(132, 23);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "ACS Connection";
            // 
            // pnLeft
            // 
            this.pnLeft.Controls.Add(this.btnManualControl);
            this.pnLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnLeft.Location = new System.Drawing.Point(0, 0);
            this.pnLeft.Name = "pnLeft";
            this.pnLeft.Size = new System.Drawing.Size(399, 303);
            this.pnLeft.TabIndex = 6;
            // 
            // btnManualControl
            // 
            this.btnManualControl.Appearance.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManualControl.Appearance.Options.UseFont = true;
            this.btnManualControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnManualControl.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btnManualControl.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnManualControl.ImageOptions.SvgImage")));
            this.btnManualControl.ImageOptions.SvgImageSize = new System.Drawing.Size(50, 50);
            this.btnManualControl.Location = new System.Drawing.Point(2, 2);
            this.btnManualControl.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.btnManualControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnManualControl.Name = "btnManualControl";
            this.btnManualControl.Size = new System.Drawing.Size(395, 299);
            this.btnManualControl.TabIndex = 0;
            this.btnManualControl.Text = "Manual Control";
            this.btnManualControl.Click += new System.EventHandler(this.btnManualControl_Click);
            // 
            // pnRight
            // 
            this.pnRight.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnRight.Controls.Add(this.btnGearSettings);
            this.pnRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnRight.Location = new System.Drawing.Point(399, 0);
            this.pnRight.Name = "pnRight";
            this.pnRight.Size = new System.Drawing.Size(401, 303);
            this.pnRight.TabIndex = 7;
            // 
            // btnGearSettings
            // 
            this.btnGearSettings.Appearance.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGearSettings.Appearance.Options.UseFont = true;
            this.btnGearSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnGearSettings.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btnGearSettings.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnGearSettings.ImageOptions.SvgImage")));
            this.btnGearSettings.ImageOptions.SvgImageSize = new System.Drawing.Size(50, 50);
            this.btnGearSettings.Location = new System.Drawing.Point(0, 0);
            this.btnGearSettings.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.btnGearSettings.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnGearSettings.Name = "btnGearSettings";
            this.btnGearSettings.Size = new System.Drawing.Size(401, 303);
            this.btnGearSettings.TabIndex = 0;
            this.btnGearSettings.Text = "Gear Settings";
            this.btnGearSettings.Click += new System.EventHandler(this.btnGearSettings_Click);
            // 
            // pnBack
            // 
            this.pnBack.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnBack.Controls.Add(this.pnRight);
            this.pnBack.Controls.Add(this.pnLeft);
            this.pnBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnBack.Location = new System.Drawing.Point(0, 267);
            this.pnBack.Name = "pnBack";
            this.pnBack.Size = new System.Drawing.Size(800, 303);
            this.pnBack.TabIndex = 8;
            // 
            // pnTop
            // 
            this.pnTop.Appearance.BackColor = System.Drawing.Color.Silver;
            this.pnTop.Appearance.Options.UseBackColor = true;
            this.pnTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnTop.Controls.Add(this.pnTopRight);
            this.pnTop.Controls.Add(this.pnTopLeft);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(800, 267);
            this.pnTop.TabIndex = 13;
            // 
            // pnTopRight
            // 
            this.pnTopRight.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnTopRight.Controls.Add(this.dataLayoutControl1);
            this.pnTopRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnTopRight.Location = new System.Drawing.Point(265, 0);
            this.pnTopRight.Name = "pnTopRight";
            this.pnTopRight.Size = new System.Drawing.Size(535, 267);
            this.pnTopRight.TabIndex = 9;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.tbAxisYStatus);
            this.dataLayoutControl1.Controls.Add(this.tbAxisXStatus);
            this.dataLayoutControl1.Controls.Add(this.tbTargetGear);
            this.dataLayoutControl1.Controls.Add(this.tbCurrentGear);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.Root;
            this.dataLayoutControl1.Size = new System.Drawing.Size(535, 267);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // tbAxisYStatus
            // 
            this.tbAxisYStatus.Location = new System.Drawing.Point(133, 48);
            this.tbAxisYStatus.Name = "tbAxisYStatus";
            this.tbAxisYStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAxisYStatus.Properties.Appearance.Options.UseFont = true;
            this.tbAxisYStatus.Properties.ReadOnly = true;
            this.tbAxisYStatus.Size = new System.Drawing.Size(390, 32);
            this.tbAxisYStatus.StyleController = this.dataLayoutControl1;
            this.tbAxisYStatus.TabIndex = 10;
            // 
            // tbAxisXStatus
            // 
            this.tbAxisXStatus.Location = new System.Drawing.Point(133, 12);
            this.tbAxisXStatus.Name = "tbAxisXStatus";
            this.tbAxisXStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAxisXStatus.Properties.Appearance.Options.UseFont = true;
            this.tbAxisXStatus.Properties.ReadOnly = true;
            this.tbAxisXStatus.Size = new System.Drawing.Size(390, 32);
            this.tbAxisXStatus.StyleController = this.dataLayoutControl1;
            this.tbAxisXStatus.TabIndex = 9;
            // 
            // tbTargetGear
            // 
            this.tbTargetGear.Location = new System.Drawing.Point(389, 84);
            this.tbTargetGear.Name = "tbTargetGear";
            this.tbTargetGear.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTargetGear.Properties.Appearance.Options.UseFont = true;
            this.tbTargetGear.Properties.ReadOnly = true;
            this.tbTargetGear.Size = new System.Drawing.Size(134, 32);
            this.tbTargetGear.StyleController = this.dataLayoutControl1;
            this.tbTargetGear.TabIndex = 8;
            // 
            // tbCurrentGear
            // 
            this.tbCurrentGear.Location = new System.Drawing.Point(131, 84);
            this.tbCurrentGear.Name = "tbCurrentGear";
            this.tbCurrentGear.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCurrentGear.Properties.Appearance.Options.UseFont = true;
            this.tbCurrentGear.Properties.ReadOnly = true;
            this.tbCurrentGear.Size = new System.Drawing.Size(142, 32);
            this.tbCurrentGear.StyleController = this.dataLayoutControl1;
            this.tbCurrentGear.TabIndex = 7;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem2,
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(535, 267);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.tbCurrentGear;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(265, 175);
            this.layoutControlItem3.Text = "Current Gear";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(114, 24);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.Control = this.tbTargetGear;
            this.layoutControlItem4.Location = new System.Drawing.Point(265, 72);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(250, 175);
            this.layoutControlItem4.Text = "Target Gear";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(107, 24);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.tbAxisXStatus;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(515, 36);
            this.layoutControlItem2.Text = "Axis Y Status";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(116, 24);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.tbAxisYStatus;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 36);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(515, 36);
            this.layoutControlItem1.Text = "Axis X Status";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(116, 24);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // pnTopLeft
            // 
            this.pnTopLeft.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnTopLeft.Controls.Add(this.labelControl6);
            this.pnTopLeft.Controls.Add(this.gaugeControl5);
            this.pnTopLeft.Controls.Add(this.labelControl5);
            this.pnTopLeft.Controls.Add(this.gaugeControl4);
            this.pnTopLeft.Controls.Add(this.labelControl4);
            this.pnTopLeft.Controls.Add(this.gaugeControl3);
            this.pnTopLeft.Controls.Add(this.labelControl1);
            this.pnTopLeft.Controls.Add(this.lbValue);
            this.pnTopLeft.Controls.Add(this.tsConvertOnOff);
            this.pnTopLeft.Controls.Add(this.gaugeControl1);
            this.pnTopLeft.Controls.Add(this.labelControl2);
            this.pnTopLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnTopLeft.Location = new System.Drawing.Point(0, 0);
            this.pnTopLeft.Name = "pnTopLeft";
            this.pnTopLeft.Size = new System.Drawing.Size(265, 267);
            this.pnTopLeft.TabIndex = 9;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(10, 109);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(96, 23);
            this.labelControl6.TabIndex = 16;
            this.labelControl6.Text = "AVL Control";
            // 
            // gaugeControl5
            // 
            this.gaugeControl5.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gaugeControl5.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge5});
            this.gaugeControl5.Location = new System.Drawing.Point(106, 102);
            this.gaugeControl5.Name = "gaugeControl5";
            this.gaugeControl5.Size = new System.Drawing.Size(36, 38);
            this.gaugeControl5.TabIndex = 15;
            // 
            // stateIndicatorGauge5
            // 
            this.stateIndicatorGauge5.Bounds = new System.Drawing.Rectangle(6, 6, 24, 26);
            this.stateIndicatorGauge5.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.siAVLControl});
            this.stateIndicatorGauge5.Name = "stateIndicatorGauge5";
            // 
            // siAVLControl
            // 
            this.siAVLControl.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(124F, 124F);
            this.siAVLControl.Name = "stateIndicatorComponent1";
            this.siAVLControl.Size = new System.Drawing.SizeF(200F, 200F);
            this.siAVLControl.StateIndex = 0;
            indicatorState21.Name = "State1";
            indicatorState21.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState22.Name = "State2";
            indicatorState22.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState23.Name = "State3";
            indicatorState23.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState24.Name = "State4";
            indicatorState24.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.siAVLControl.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState21,
            indicatorState22,
            indicatorState23,
            indicatorState24});
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(10, 176);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(42, 23);
            this.labelControl5.TabIndex = 14;
            this.labelControl5.Text = "Error";
            // 
            // gaugeControl4
            // 
            this.gaugeControl4.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gaugeControl4.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge4});
            this.gaugeControl4.Location = new System.Drawing.Point(47, 169);
            this.gaugeControl4.Name = "gaugeControl4";
            this.gaugeControl4.Size = new System.Drawing.Size(42, 38);
            this.gaugeControl4.TabIndex = 13;
            // 
            // stateIndicatorGauge4
            // 
            this.stateIndicatorGauge4.Bounds = new System.Drawing.Rectangle(6, 6, 30, 26);
            this.stateIndicatorGauge4.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.siError});
            this.stateIndicatorGauge4.Name = "stateIndicatorGauge4";
            // 
            // siError
            // 
            this.siError.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(124F, 124F);
            this.siError.Name = "stateIndicatorComponent1";
            this.siError.Size = new System.Drawing.SizeF(200F, 200F);
            this.siError.StateIndex = 0;
            indicatorState25.Name = "State1";
            indicatorState25.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState26.Name = "State2";
            indicatorState26.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState27.Name = "State3";
            indicatorState27.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState28.Name = "State4";
            indicatorState28.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.siError.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState25,
            indicatorState26,
            indicatorState27,
            indicatorState28});
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(10, 143);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(83, 23);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "E-Stop Bit";
            // 
            // gaugeControl3
            // 
            this.gaugeControl3.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gaugeControl3.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge3});
            this.gaugeControl3.Location = new System.Drawing.Point(92, 137);
            this.gaugeControl3.Name = "gaugeControl3";
            this.gaugeControl3.Size = new System.Drawing.Size(40, 37);
            this.gaugeControl3.TabIndex = 11;
            // 
            // stateIndicatorGauge3
            // 
            this.stateIndicatorGauge3.Bounds = new System.Drawing.Rectangle(6, 6, 28, 25);
            this.stateIndicatorGauge3.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.siEStopBit});
            this.stateIndicatorGauge3.Name = "stateIndicatorGauge3";
            // 
            // siEStopBit
            // 
            this.siEStopBit.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(124F, 124F);
            this.siEStopBit.Name = "stateIndicatorComponent1";
            this.siEStopBit.Size = new System.Drawing.SizeF(200F, 200F);
            this.siEStopBit.StateIndex = 0;
            indicatorState29.Name = "State1";
            indicatorState29.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState30.Name = "State2";
            indicatorState30.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState31.Name = "State3";
            indicatorState31.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState32.Name = "State4";
            indicatorState32.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.siEStopBit.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState29,
            indicatorState30,
            indicatorState31,
            indicatorState32});
            // 
            // lbValue
            // 
            this.lbValue.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbValue.Appearance.Options.UseFont = true;
            this.lbValue.Location = new System.Drawing.Point(10, 44);
            this.lbValue.Name = "lbValue";
            this.lbValue.Size = new System.Drawing.Size(182, 23);
            this.lbValue.TabIndex = 8;
            this.lbValue.Text = "Digital Input Value : -";
            // 
            // tsConvertOnOff
            // 
            this.tsConvertOnOff.Enabled = false;
            this.tsConvertOnOff.Location = new System.Drawing.Point(107, 7);
            this.tsConvertOnOff.Name = "tsConvertOnOff";
            this.tsConvertOnOff.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsConvertOnOff.Properties.Appearance.Options.UseFont = true;
            this.tsConvertOnOff.Properties.OffText = "Off";
            this.tsConvertOnOff.Properties.OnText = "On";
            this.tsConvertOnOff.Size = new System.Drawing.Size(152, 29);
            this.tsConvertOnOff.TabIndex = 6;
            this.tsConvertOnOff.Toggled += new System.EventHandler(this.tsConvertOnOff_Toggled);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(10, 10);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(91, 23);
            this.labelControl2.TabIndex = 7;
            this.labelControl2.Text = "Conversion";
            // 
            // timerUpdate
            // 
            this.timerUpdate.Interval = 1000;
            this.timerUpdate.Tick += new System.EventHandler(this.timerUpdate_Tick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 570);
            this.Controls.Add(this.pnBack);
            this.Controls.Add(this.pnTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.Text = "Main";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.SizeChanged += new System.EventHandler(this.Main_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.siACSConnection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnLeft)).EndInit();
            this.pnLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnRight)).EndInit();
            this.pnRight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnBack)).EndInit();
            this.pnBack.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnTop)).EndInit();
            this.pnTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnTopRight)).EndInit();
            this.pnTopRight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbAxisYStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbAxisXStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTargetGear.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCurrentGear.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnTopLeft)).EndInit();
            this.pnTopLeft.ResumeLayout(false);
            this.pnTopLeft.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.siAVLControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.siError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.siEStopBit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tsConvertOnOff.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl1;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge1;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent siACSConnection;
        private DevExpress.XtraEditors.PanelControl pnRight;
        private DevExpress.XtraEditors.PanelControl pnLeft;
        private DevExpress.XtraEditors.PanelControl pnBack;
        private DevExpress.XtraEditors.PanelControl pnTop;
        private DevExpress.XtraEditors.SimpleButton btnGearSettings;
        private DevExpress.XtraEditors.SimpleButton btnManualControl;
        private DevExpress.XtraEditors.LabelControl lbValue;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ToggleSwitch tsConvertOnOff;
        private DevExpress.XtraEditors.PanelControl pnTopRight;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraEditors.TextEdit tbTargetGear;
        private DevExpress.XtraEditors.TextEdit tbCurrentGear;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit tbAxisYStatus;
        private DevExpress.XtraEditors.TextEdit tbAxisXStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.Timer timerUpdate;
        private DevExpress.XtraEditors.PanelControl pnTopLeft;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl3;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge3;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent siEStopBit;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl5;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge5;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent siAVLControl;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl4;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge4;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent siError;
    }
}

