﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using System.Diagnostics;

namespace AVL_GearShifter
{
    public partial class GearSettings : DevExpress.XtraEditors.XtraForm
    {
        GearInfo gearInfo;
        public GearSettings(GearInfo gearInfo)
        {
            InitializeComponent();
            this.gearInfo = gearInfo;
            var geartypes = gearInfo.GetGearTypes();

            if (geartypes == null)
            {
                return;
            }

            foreach (GearType gt in geartypes)
            {
                cbGearTypes.Properties.Items.Add(gt.Name);
            }

            cbGearTypes.SelectedIndex = gearInfo.gearTypes.CurrentGearTypeNumber;


            this.FormClosing += (s, e) => ValidateChildren();
        }

        private void GearSettings_SizeChanged(object sender, EventArgs e)
        {
            btnHoming.Width = btnControlOn.Width = btnControlOff.Width = pnTopLeftLeft.Width / 3;
        }

        private void timerUpdate_Tick(object sender, EventArgs e)
        {
            tbAxisXStatus.Text = gearInfo.CurrentGearStatus.AxisXStatusWord.enumStatusWord.ToString();
            tbAxisYStatus.Text = gearInfo.CurrentGearStatus.AxisYStatusWord.enumStatusWord.ToString();
            tbAxisXCurrentPosition.Text = gearInfo.CurrentGearStatus.AxisXActualPosition.ToString();
            tbAxisYCurrentPosition.Text = gearInfo.CurrentGearStatus.AxisYActualPosition.ToString();
        }

        private void btnHoming_Click(object sender, EventArgs e)
        {
            gearInfo.Homing();
        }

        private void btnControlOn_Click(object sender, EventArgs e)
        {
            gearInfo.ControlOn();
        }

        private void btnControlOff_Click(object sender, EventArgs e)
        {
            gearInfo.ControlOff();
        }
        private void btnAddGearType_Click(object sender, EventArgs e)
        {
            var result = XtraInputBox.Show("새로 생성할 기어 타입의 이름을 입력해주세요.", "<size=14>Add New Gear Type</size>", "");

            if (result == "")
            {
                return;
            }
            cbGearTypes.Properties.Items.Add(result);
            var gearTypes = gearInfo.GetGearTypes();
            gearTypes.Add(new GearType(result));
            cbGearTypes.SelectedIndex = cbGearTypes.Properties.Items.Count - 1;
        }

        private void btnDeleteGearType_Click(object sender, EventArgs e)
        {
            var result = XtraMessageBox.Show($"<size=14>현재 기어 타입을 삭제합니다. 삭제하시겠습니까?</size>", "<size=14>기어 삭제</size>", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result == DialogResult.OK)
            {
                var gearTypes = gearInfo.GetGearTypes();
                gearTypes.RemoveAt(cbGearTypes.SelectedIndex);
                cbGearTypes.Properties.Items.RemoveAt(cbGearTypes.SelectedIndex);
                cbGearTypes.SelectedIndex = 0;

                gridControl1.BeginUpdate();
                gridControl1.EndUpdate();

            }

        }
        private void btnAddGear_Click(object sender, EventArgs e)
        {
            var result = XtraInputBox.Show("새로 생성할 기어의 이름을 입력해주세요.", "<size=14>Add New Gear</size>", "");

            if (result == "")
            {
                return;
            }


            var gearTypes = gearInfo.GetGearTypes();
            GearPosition gp = new GearPosition();
            gp.SetGearPosition(result, 0, 0, null, null, false, false, false);
            gearTypes[gearInfo.gearTypes.CurrentGearTypeNumber].GearPositionList.Add(gp);

            gridControl1.BeginUpdate();
            gridControl1.EndUpdate();
        }

        private void btnDeleteGear_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0)
            {
                return;
            }

            var gearTypes = gearInfo.GetGearTypes();
            string gearName = gearTypes[gearInfo.gearTypes.CurrentGearTypeNumber].GearPositionList[gridView1.FocusedRowHandle].Name;
            var result = XtraMessageBox.Show($"<size=14>{gearName}을 삭제합니다. 삭제하시겠습니까?</size>", "<size=14>기어 삭제</size>", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result == DialogResult.OK)
            {
                gearTypes[gearInfo.gearTypes.CurrentGearTypeNumber].GearPositionList.RemoveAt(gridView1.FocusedRowHandle);
            }
            
        }

        private void cbGearTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            var gearTypes = gearInfo.GetGearTypes();
            gearInfo.gearTypes.CurrentGearTypeNumber = cbGearTypes.SelectedIndex;
            gridControl1.DataSource = gearTypes[gearInfo.gearTypes.CurrentGearTypeNumber].GearPositionList;

            gearInfo.EmptyTargetGear();

        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (e.Column.FieldName.Equals("StringPrevGear") || e.Column.FieldName.Equals("StringNextGear"))
            {
                var currentGearType = gearInfo.GetGearTypes()[gearInfo.gearTypes.CurrentGearTypeNumber];

                SelectPrevandNextGears form = new SelectPrevandNextGears(currentGearType, gridView1.FocusedRowHandle, e.Column.FieldName);
                form.ShowDialog();
            }
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            AssignCurrentPositionToSelectedGearPosition(gridView1.FocusedRowHandle);
        }

        private void AssignCurrentPositionToSelectedGearPosition(int SelectedRowNumber)
        {
            var currentGearType = gearInfo.GetGearTypes()[gearInfo.gearTypes.CurrentGearTypeNumber];
            currentGearType.GearPositionList[gridView1.FocusedRowHandle].ActualPositionX = gearInfo.CurrentGearStatus.AxisXActualPosition;
            currentGearType.GearPositionList[gridView1.FocusedRowHandle].ActualPositionY = gearInfo.CurrentGearStatus.AxisYActualPosition;

            gridControl1.BeginUpdate();
            gridControl1.EndUpdate();
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {

        }

        private void GearSettings_FormClosed(object sender, FormClosedEventArgs e)
        {
            gearInfo.SetCurrentGearType();

        }

        private void gridView1_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            //var currentGearType = gearInfo.GetGearTypes()[gearInfo.gearTypes.CurrentGearTypeNumber];
            //if (e.Column.FieldName == "BacktoPrev")
            //{
            //    currentGearType.GearPositionList[gridView1.FocusedRowHandle].BacktoPrev = (bool)e.Value;
            //}
            //else if (e.Column.FieldName == "BacktoNext")
            //{
            //    currentGearType.GearPositionList[gridView1.FocusedRowHandle].BacktoNext = (bool)e.Value;
            //}
            //else if (e.Column.FieldName == "Usage")
            //{
            //    currentGearType.GearPositionList[gridView1.FocusedRowHandle].Usage = (bool)e.Value;
            //}

        }
    }
}