﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;

namespace AVL_GearShifter
{
    public partial class SelectPrevandNextGears : DevExpress.XtraEditors.XtraForm
    {
        GearType currentGearType;
        string columnFieldName;
        int selectedGearPosition;
        DataTable dt;
        public SelectPrevandNextGears(GearType currentGearType, int selectedGearPosition, string columnFieldName)
        {
            InitializeComponent();
            this.currentGearType = currentGearType;
            this.columnFieldName = columnFieldName;
            this.selectedGearPosition = selectedGearPosition;

            if (columnFieldName == "StringPrevGear")
            {
                this.Text = "Select Prev Gear";
            }
            else if (columnFieldName == "StringNextGears")
            {
                this.Text = "Select Next Gears";
            }

            lbSelectedGearPosition.Text = "Selected Gear Name : " + currentGearType.GearPositionList[selectedGearPosition].Name;

            dt = new DataTable("GearList");
            dt.Columns.Add("-", typeof(bool));
            dt.Columns.Add("GearName", typeof(string));

            gridView1.OptionsSelection.CheckBoxSelectorField = "-";
            
            CheckHavingGearPosition();

            gridControl1.DataSource = dt;

            gridControl1.BeginUpdate();
            gridControl1.EndUpdate();

        }

        private void CheckHavingGearPosition()
        {
            if (columnFieldName == "StringPrevGear")
            {
                foreach (GearPosition gp in currentGearType.GearPositionList)
                {
                    if (gp.Name == currentGearType.GearPositionList[selectedGearPosition].Name)
                    {
                        continue;
                    }

                    dt.Rows.Add(new object[] { false, gp.Name });

                    if (currentGearType.GearPositionList[selectedGearPosition].Prev == null)
                    {
                        continue;
                    }

                    bool check = false;
                    if (gp.Name == currentGearType.GearPositionList[selectedGearPosition].Prev.Name)
                    {
                        check = true;
                    }

                    dt.Rows[dt.Rows.Count - 1]["-"] = check;

                }

            }
            else if (columnFieldName == "StringNextGear")
            {
                foreach (GearPosition gp in currentGearType.GearPositionList)
                {
                    if (gp.Name == currentGearType.GearPositionList[selectedGearPosition].Name)
                    {
                        continue;
                    }

                    dt.Rows.Add(new object[] { false, gp.Name });

                    if (currentGearType.GearPositionList[selectedGearPosition].Next == null)
                    {
                        continue;
                    }

                    bool check = false;
                    foreach (GearPosition nextGp in currentGearType.GearPositionList[selectedGearPosition].Next)
                        if (gp.Name == nextGp.Name)
                        {
                            check = true;
                        }

                    dt.Rows[dt.Rows.Count - 1]["-"] = check;

                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SelectPrevandNextGears_FormClosed(object sender, FormClosedEventArgs e)
        {
            SavePrevNextGearPosition();
        }

        private void SavePrevNextGearPosition()
        {
            bool FlagNotCheck = true;

            if (columnFieldName == "StringPrevGear")
            {
                int[] selectedRows = gridView1.GetSelectedRows();

                for (int i = 0; i < selectedRows.Length; i++)
                {
                    string checkGearName = dt.Rows[selectedRows[i]].ItemArray[1].ToString();

                    FlagNotCheck = false;
                    foreach (GearPosition gp in currentGearType.GearPositionList)
                    {
                        if (gp.Name == checkGearName)
                        {
                            currentGearType.GearPositionList[selectedGearPosition].Prev = gp;
                        }
                    }
                }
                if (FlagNotCheck)
                {
                    currentGearType.GearPositionList[selectedGearPosition].Prev = null;
                }
            }
            else if (columnFieldName == "StringNextGear")
            {
                if (currentGearType.GearPositionList[selectedGearPosition].Next == null)
                {
                    currentGearType.GearPositionList[selectedGearPosition].Next = new List<GearPosition>();
                }

                currentGearType.GearPositionList[selectedGearPosition].Next.Clear();

                int[] selectedRows = gridView1.GetSelectedRows();

                for (int i = 0; i < selectedRows.Length; i++)
                {
                    string checkGearName = dt.Rows[selectedRows[i]].ItemArray[1].ToString();

                    FlagNotCheck = false;

                    foreach (GearPosition gp in currentGearType.GearPositionList)
                    {
                        if (gp.Name == checkGearName)
                        {

                            currentGearType.GearPositionList[selectedGearPosition].Next.Add(gp);
                        }
                    }
                }
                if (FlagNotCheck)
                {
                    currentGearType.GearPositionList[selectedGearPosition].Next = null;
                }
            }
        }
        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (columnFieldName == "StringPrevGear")
            {
                GridView view = sender as GridView;
                if (e.Action == System.ComponentModel.CollectionChangeAction.Add && view.GetSelectedRows().Length > 1)
                    view.ClearSelection();
                view.SelectRow(view.GetRowHandle(e.ControllerRow));
            }
        }
    }

}